# CompCreate.jl

[![Build Status](https://ci.appveyor.com/api/projects/status/github/saersamani/CompCreate.jl?svg=true)](https://ci.appveyor.com/project/saersamani/CompCreate-jl)


 **CompCreate.jl** is a julia package for analysis of the High Resolution Mass Spectrometry (HRMS) coupled with Liquid Chromatography (LC) data. The algorithm developed within the frameworks of julia 1.6.2 and the current implementation is only applicable to profile HRMS data acquired in data independent mode. The **CompCreate.jl** creates components (i.e. unique chemical signal) by grouping several features such as adducts, isotopes, *in-source* fragments in the MS1 signal and the fragments in the MS2 signal into one component. The algorithm is explained in details [elsewhere](https://pubs.acs.org/doi/abs/10.1021/acs.est.8b00259).

 Please cite when using CompCreate.jl for you own work.
 Samanipour et al. *Environ. Sci. Technol.* 2018, 52, 8, 4694–4701.

## Installation

Given that **CompCreate.jl** is not a registered julia package, for installation the *url* to the repository and the julia package manager are necessary.

```julia
using Pkg
Pkg.add(PackageSpec(url="https://bitbucket.org/SSamanipour/compcreate.jl/src/master/"))


```

## Usage
There are two main functions implemented via **CompCreate.jl**, namely: *comp_ms1(--)* and *compcreate(--)*. These functions enable the automated generation of components based on the feature lists created by [**SAFD.jl**](https://bitbucket.org/SSamanipour/safd.jl/src/master/) and the MS2 data incorporated in mzXML files. Please make sure that the feature lists are generated with **SAFD** version 0.5.16 and later.

### compcreate(--)
This function independently selects the appropriate algorithm for the componentization of DIA, DDA, and MS1 only datasets. The inputs of this function are the same as *comp_DIA(--)*.

```julia
using CompCreate
using JLD2 # This package is necessary for loading the Auxiliary file

components = compcreate(chrom::Dict{Any},path2features::String,mass_win_per::Float64,
    ret_win_per::Float64,r_thresh::Float64,delta_mass::Float64,min_int::Float64)

```


###  comp_ms1(--)
This function performs the componentization only on the MS1 level and saves the report as a CSV file in the same folder as the feature list.
```julia
using CompCreate
using JLD2 # This package is necessary for loading the Auxiliary file

components = comp_ms1(chrom::Dict{Any},path2features::String,mass_win_per::Float64,
    ret_win_per::Float64,r_thresh::Float64,delta_mass::Float64)


```
#### Inputs comp_ms1(--)
* **chrom::Dict{Any}** is a julia dictionary which contains all the relevant information related to the LC-MS run extracted from the mzXML file. If the package [**MS_Import.jl**](https://bitbucket.org/SSamanipour/ms_import.jl/src/master/) is used for parsing the mzXML files, the output automatically has the correct format. In case other HRMS data importing methods are used the final Dict must have the below structure.

    + chrom
        + MS1
            + Mz_values: an m \* n  matrix with m being the scan numbers and n being the number of mass channels, which contains the measured m/z values.
            + Mz_intensity: an m \* n  matrix with m being the scan numbers and n being the number of mass channels, which contains the intensity associated with each measured m/z value.
            + t0: the start of the chromatographic run.
            + t_end: the end of chromatographic run.
            + Polarity: the ionization mode (i.e. "+" and/or "-")
            + TIC: the total ion current
            + BasePeak: the base peak for each scan
            + PrecursorIon: the precursor ion for each scan
            + PrecursorIonWin: the width of the mass window for the precursor ion


        + MS2
            + Mz_values: an m \* n  matrix with m being the scan numbers and n being the number of mass channels, which contains the measured m/z values.
            + Mz_intensity: an m \* n  matrix with m being the scan numbers and n being the number of mass channels, which contains the intensity associated with each measured m/z value.
            + t0: the start of the chromatographic run.
            + t_end: the end of chromatographic run.
            + Polarity: the ionization mode (i.e. "+" and/or "-")
            + TIC: the total ion current
            + BasePeak: the base peak for each scan
            + PrecursorIon: the precursor ion for each scan
            + PrecursorIonWin: the width of the mass window for the precursor ion


* **path2features::String** is the path to a CSV file that contains the feature list. An example of such file is available in the [test folder](https://bitbucket.org/SSamanipour/compcreate.jl/src/master/test/test_chrom_report.csv).

* **mass_win_per::Float64 (0.01-1)** is the percentage of the peak width in the mass domain used for the calculation of mass tolerance. The typical value for this parameter with a nominal mass resolution of 20000 is 0.75.

* **ret_win_per::Float64 (0.01-1)** is the percentage of the peak with in the time domain used for the calculation of retention time tolerance. The typical value for this parameter with a nominal mass resolution of 20000 is 0.5.  

* **r_thresh::Float64 (0.5-1)** is the correlation coefficient threshold used during the comparison of the peak shape of the parent ion and the fragments. The typical value for this parameter is 0.85.


* **delta_mass::Float64 (0-0.01)** is the KMD deviation used for the detection of isotopes. A value of 0.004 is associated with less than 5% rate of false positive detection.

#### Output comp_ms1(--)

This function generates a DataFrame with the components as well as a CSV file with the components saved in the folder of the feature_list. An example of such file is available in the [test folder](https://bitbucket.org/SSamanipour/compcreate.jl/src/master/test/test_chrom_report_compMS1.csv)


### compcreate(--)
This function performs the componentization on both MS1 and MS2 levels for LC-HRMS data. The final components are saved in a report as a CSV file in the same folder as the feature list.

```julia
using CompCreate
using JLD2 # This package is necessary for loading the Auxiliary file

components = compcreate(chrom::Dict{Any},path2features::String,mass_win_per::Float64,
    ret_win_per::Float64,r_thresh::Float64,delta_mass::Float64,min_int::Float64)

```




#### Inputs compcreate(--)
* **chrom::Dict{Any}** is a julia dictionary which contains all the relevant information related to the LC-MS run extracted from the mzXML file. If the package [**MS_Import.jl**](https://bitbucket.org/SSamanipour/ms_import.jl/src/master/) is used for parsing the mzXML files, the output automatically has the correct format. In case other HRMS data importing methods are used the final Dict must have the below structure.


    + chrom
        + MS1
            + Mz_values: an m \* n  matrix with m being the scan numbers and n being the number of mass channels, which contains the measured m/z values.
            + Mz_intensity: an m \* n  matrix with m being the scan numbers and n being the number of mass channels, which contains the intensity associated with each measured m/z value.
            + t0: the start of the chromatographic run.
            + t_end: the end of chromatographic run.
            + Polarity: the ionization mode (i.e. "+" and/or "-")
            + TIC: the total ion current
            + BasePeak: the base peak for each scan
            + PrecursorIon: the precursor ion for each scan
            + PrecursorIonWin: the width of the mass window for the precursor ion


        + MS2
            + Mz_values: an m \* n  matrix with m being the scan numbers and n being the number of mass channels, which contains the measured m/z values.
            + Mz_intensity: an m \* n  matrix with m being the scan numbers and n being the number of mass channels, which contains the intensity associated with each measured m/z value.
            + t0: the start of the chromatographic run.
            + t_end: the end of chromatographic run.
            + Polarity: the ionization mode (i.e. "+" and/or "-")
            + TIC: the total ion current
            + BasePeak: the base peak for each scan
            + PrecursorIon: the precursor ion for each scan
            + PrecursorIonWin: the width of the mass window for the precursor ion


* **path2features::String** is the path to a CSV file that contains the feature list. An example of such file is available in the [test folder](https://bitbucket.org/SSamanipour/compcreate.jl/src/master/test/test_chrom_report.csv).

* **mass_win_per::Float64 (0.01-1)** is the percentage of the peak with in the mass domain used for the calculation of mass tolerance. The typical value for this parameter with a nominal mass resolution of 20000 is 0.75.

* **ret_win_per::Float64 (0.01-1)** is the percentage of the peak with in the time domain used for the calculation of retention time tolerance. The typical value for this parameter with a nominal mass resolution of 20000 is 0.75.  

* **r_thresh::Float64 (0.5-1)** is the correlation coefficient threshold used during the comparison of the peak shape of the parent ion and the fragments. The typical value for this parameter is 0.85.

* **delta_mass::Float64 (0-0.01)** is the KMD deviation used for the detection of isotopes. A value of 0.004 is associated with less than 5% rate of false positive detection.

* **min_int::Float64 (0-Inf)** is the minimum intensity of the MS2 ions to be considered meaningful (i.e. not noise).    

#### Output compcreate(--)

This function generates a DataFrame with the components as well as a CSV file with the components saved in the folder of the feature_list. An example of such file is available in the [test folder](https://bitbucket.org/SSamanipour/compcreate.jl/src/master/test/test_chrom_report_comp.csv).


### Examples

For more examples on how to process your own data please take a look at folder [examples](https://bitbucket.org/SSamanipour/compcreate.jl/src/master/examples/).


## Contribution

Issues and pull requests are welcome! Please contact the developers for further information.
