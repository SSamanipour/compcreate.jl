
using CompCreate
using MS_Import
using JLD


#############################################################################
# Wrapping function for componentization batch

function comp_DIA_batch(pathin,mass_win_per,ret_win_per,r_thresh,delta_mass,min_int,mz_thresh)

    fs=readdir(pathin)
    for i=1:length(fs)
        fs1=split(fs[i],'.')
        if fs1[2] == "csv"
            tv1=fs1[1]
            filenames=[string(tv1[1:end-7],".mzXML")]
            println(filenames)
            path2features=joinpath(pathin,fs[i])
            chrom=import_files(pathin,filenames,mz_thresh)

            comp_DIA(chrom,path2features,mass_win_per,ret_win_per,r_thresh,delta_mass,min_int)

        end


    end


end # function

###############################################################################
# Parameters

# File importing
pathin="/path/to/the/folder/with/both/feature_list/and/mzXML_files"
mz_thresh=[0,600]

# Component creation
mass_win_per=0.8
ret_win_per=0.5
r_tresh=0.8
delta_mass=0.004
min_int=300

comp_DIA_batch(pathin,mass_win_per,ret_win_per,r_tresh,delta_mass,min_int,mz_thresh)
