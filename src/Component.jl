using CSV
using DataFrames
using Statistics
#using JLD
using HDF5
using ProgressBars
using JLD2
using FileIO
#using ProgressMeter
using Pkg

using BSON 

############################################################
# error reporting
function catch_error(error,iteration)
    #msg = sprint(showerror, e, catch_backtrace

    println("Please report the following:")
    println("----------------------------------")

    deps = Pkg.dependencies()
    for (uuid, dep) in deps
        if dep.name == "CompCreate"
            println("CompCreate version : "*string(dep.version))
        end
    end
    println("Fail at iteration : "*string(floor(iteration)))
    @error "Task Failed" exception=(error, catch_backtrace())
    rethrow()

end

#####################################################################################
# Replacing findall for optimization
function comp_findall(m_min,m_max,comp_vec)
    high = size(comp_vec)[1]

    function search_indexed_loop(low,high,ind)

        if low==(high-1)
            return low
        end

        #middle = div((high+low),2,RoundNearest) #<-slighlty slower
        #middle = round(high+low;digits=-1, base = 2) #<-slowest
        #middle = Int(floor((low+high)/2)) #<-fastest
        middle = div((high+low),2) #<-slighlty slower

        if comp_vec[middle] > ind || comp_vec[middle] == 0
            return search_indexed_loop(low,middle,ind)
        else
            return search_indexed_loop(middle,high,ind)
        end

    end


    t2=search_indexed_loop(1,high,m_max)

    # if index 2 is the first index then no index smaller, array is empty.
    if t2 == 1
        return Int64[]
    end
    #= next if condition is for optimization. quite often empty index vectors
    are found, to skip the search index, the following is done:
    if the value of the maximum index is smaller than the minimum searched
    value then no index found, and empty array can be returned =#
    if comp_vec[t2] < m_min

      #println("flag3")
      return Int64[]
    else

      #=given I found max, no need to look for anything over max
      so looking at the array bellow it, which is why I search for max
      first.=#

      t=search_indexed_loop(1,t2,m_min) #max(1,t2-30)

      return (((t != 1 || comp_vec[1] < m_min) ? t+1 : 1):t2)

    end
end



########################################################################
# XIC extract

# mz_v = ms2val
# mz_i = ms2int
# mass = tr_m_s[i]

function xic_extract(mz_v,mz_i,mass,mass_tol)

    md = abs.(mz_v .- mass);
    mz_i1 = deepcopy(mz_i)
    mz_i1[md .>= mass_tol] .=0

    m_rep = mean(mz_v[mz_i1 .>0])
    mz_v[mz_i1 .>0] .=0

    xic = maximum(mz_i1,dims=2)
    return(xic,m_rep,mz_v)

end


##############################################################################
#

function xic_gen(ms_val,ms_int,feature)

    ms_int_tm = deepcopy(ms_int)
    dms = abs.(ms_val .- feature[:MeasMass])
    ms_int_tm[ dms .> (feature[:MaxMass]-feature[:MinMass])/2] .= 0
    xic = maximum(ms_int_tm,dims=2)

    return xic

end # function


##############################################################################
# Correlation matrix generation MS1

function corr_mat_ms1(chrom,sel_features,feature)


    ret_win=maximum(sel_features[:,:ScanInPeak])/2

    if floor(feature[:ScanNum]-ret_win) > 0 && ceil(feature[:ScanNum]+ret_win) <= size(chrom["MS1"]["Mz_values"],1)

        ms_val=chrom["MS1"]["Mz_values"][convert(Int32,floor(feature[:ScanNum]-ret_win)):convert(Int32,ceil(feature[:ScanNum]+ret_win)),:]
        ms_int=chrom["MS1"]["Mz_intensity"][convert(Int32,floor(feature[:ScanNum]-ret_win)):convert(Int32,ceil(feature[:ScanNum]+ret_win)),:]
    elseif floor(feature[:ScanNum]-ret_win) <= 0
        ms_val=chrom["MS1"]["Mz_values"][1:convert(Int32,ceil(feature[:ScanNum]+ret_win)),:]
        ms_int=chrom["MS1"]["Mz_intensity"][1:convert(Int32,ceil(feature[:ScanNum]+ret_win)),:]
    elseif ceil(feature[:ScanNum]+ret_win) > size(chrom["MS1"]["Mz_values"],1)
        ms_val=chrom["MS1"]["Mz_values"][convert(Int32,floor(feature[:ScanNum]-ret_win)):end,:]
        ms_int=chrom["MS1"]["Mz_intensity"][convert(Int32,floor(feature[:ScanNum]-ret_win)):end,:]
    end


    corr_mat=zeros(size(ms_val,1),size(sel_features,1)+1);
    corr_mat[:,1]=sma(xic_gen(ms_val,ms_int,feature),3)

    for i=2:size(sel_features,1)+1
        corr_mat[:,i]=sma(xic_gen(ms_val,ms_int,sel_features[i-1,:]),3)

    end
    return corr_mat

end # function

##############################################################################
# Correlation checking

function corr_check(corr_mat,r_thresh)
    if size(corr_mat)[1] .== length(corr_mat)
        #only the MS1 precursor will be in the corr_mat
        return r_ind = []
    end

    r=cor(corr_mat,dims=1)
    r1=round.(r[2:end,1];digits=2)

    r_ind=findall(x -> x >=r_thresh,r1)
    return r_ind
end # function



##############################################################################
# Adduct finding

function adduct_find(pr,sel_features_cor,mode,mass_win_per,add_m)

    if mode == "POSITIVE"
        mass=sel_features_cor.MeasMass[sel_features_cor.Nr .== pr][1]- 1.007825
    elseif mode == "NEGATIVE"
        mass=sel_features_cor.MeasMass[sel_features_cor.Nr .== pr][1] + 1.007825
    end

    dmass=sel_features_cor.MeasMass .- mass
    mass_tol=mass_win_per*(sel_features_cor.MaxMass[sel_features_cor.Nr .== pr] -
    sel_features_cor.MinMass[sel_features_cor.Nr .== pr])

    x=zeros(length(dmass),1)
    add=zeros(length(dmass),1)

    for i=1:length(dmass)
        err=abs.(abs(dmass[i]) .- add_m)
        tv=findall(x -> x <= mass_tol[1], err)
        if length(tv)>0
            x[i]=1
            add[i]=add_m[tv[1]]
        end
    end

    return(x,add,mass)
end # function


##############################################################################
# finding isotopes

function iso_find(pr,sel_features_cor,delta_mass,ru_m,EMDmodel)

    mass=sel_features_cor.MeasMass[sel_features_cor.Nr .== pr][1]

    x=zeros(size(sel_features_cor,1),1)
    kmd_p=KMD_calc_ext(mass,ru_m["ru_m"])

    # #model
    if !isempty(EMDmodel)
        er_names = split(EMDmodel["er_names"][1]," ")
        er_masses = EMDmodel["er_masses"]
    end



    for i=1:size(sel_features_cor,1)
        #println(i)
        if sel_features_cor.MeasMass[i] >= mass && sel_features_cor.MeasMass[i] - mass <= 5*1.003
            if isempty(EMDmodel)
                kmd_temp=KMD_calc_ext(sel_features_cor.MeasMass[i],ru_m["ru_m"])
                m_tol=0.2*(sel_features_cor.MaxMass[i]-sel_features_cor.MinMass[i])
                if round(median(abs.(kmd_p[1] .- kmd_temp[1])),digits=3) <= delta_mass + m_tol
                    x[i]=1
                end
            else
                score = calcEMDscore(mass, sel_features_cor.MeasMass[i], EMDmodel, er_names, er_masses)
                # println("EMD $score")
                if score[1] >=  0.9997
                    x[i] = 1
                end
            end
        end

    end

    return x

end # function

function calcEMDscore(massParent, massIsotope, probDist, er_names, er_masses)
    #calculates the EMD score for the parent-isotope pair
    EMDscore = ones(length(massIsotope)).*NaN

    #calculate EMD values
    EMD = EMD_calc_ext([massParent massIsotope],er_masses)

    #calc scores
    pdistbins = vec([probDist["dER"]' 1])
    keynames = ["tp"*er_names[1] "tn"*er_names[1]]
    for k = 2:length(er_names)
        keynames = [keynames "tp"*er_names[k] "tn"*er_names[k]]
    end
    for i = 1:length(EMDscore)
        dEMD = EMD[i+1] .- EMD[1]
        pTP = 1 #start with a probability of 1
        pTN = 1
        for p = 1:length(er_names)
            ind = findlast(pdistbins .< dEMD[p])
            if ind == nothing
                ind = 1
            end
            #calculate true positive percentage probability
            pTP = pTP * 100 * (probDist[keynames[1+(p*2-2)]][ind])
            #calculate false negative percentage probability
            pTN = pTN * 100 * (probDist[keynames[2+(p*2-2)]][ind])
        end
        EMDscore[i] = 1 .- (pTN)./(pTP)
    end

    return EMDscore
end


function EMD_calc_ext(mz_values,m_ru)

    ER=Array{Any}(undef,length(mz_values))
    for i=1:length(mz_values)
        KM=mz_values[i].*(round.(m_ru)./m_ru) # Kendrick mass
        ER[i]=round.(round.(KM) .- KM ; digits=3)
    end

    return ER
end


##############################################################################
# finding in-source fragments

function in_source_frag_find(pr,sel_features_cor,NL,mass_win_per)
    x=zeros(size(sel_features_cor,1),1)
    mass_err=mass_win_per*(sel_features_cor.MaxMass[sel_features_cor.Nr .== pr] -
    sel_features_cor.MinMass[sel_features_cor.Nr .== pr])/2
    mass=sel_features_cor.MeasMass[sel_features_cor.Nr .== pr]
    n_ls=mass .- sel_features_cor.MeasMass


    for i=1:size(sel_features_cor,1)
        # println("CNL")
        if n_ls[i] == 0
            x[i]=1

        elseif n_ls[i] > 0 && any(abs.(n_ls[i] .- NL["mass"]) .<= mass_err) &&
                NL["prob"][argmin(abs.(n_ls[i] .- NL["mass"]))] >= 0.05
            x[i]=1

        end
    end

    return x

end # function

##############################################################################
# MS1 features cleanup

function feature_list_clean(sel_features,feature,add_m)
    dm = sel_features[!,:MeasMass] .- feature[:MeasMass]
    sel_features_c = sel_features[dm .<= maximum(abs.(add_m)),:]

    return sel_features_c
end


##############################################################################
# finding MS1 Component

function ms1_comp_finder(feature,feature_list,chrom,ret_win_per,r_thresh,
    delta_mass,add_m,ru_m,NL,mode,mass_win_per,Ctype,EMDmodel)


    # ret_win=ceil(feature[:ScanInPeak]*ret_win_per)
    if feature[:MinInPeak] > 0
        ret_win=feature[:MinInPeak]*ret_win_per*2
    else
        ret_win=0.05*ret_win_per*2
    end

    ind_parent=[]
    ind_ms1_comp=[]

    if ret_win >= feature[:Rt]
        sel_features_=feature_list[findall(x -> feature[:Rt]+ret_win >= x > 0 ,feature_list[:,:Rt]),:]
    else
        sel_features_=feature_list[findall(x -> feature[:Rt]+ret_win >= x >= feature[:Rt]-ret_win
        ,feature_list[:,:Rt]),:]
    end

    if Ctype == "GC"
        #No adducts, therefore maximum Measmass is precursor mass +10
        sel_features_ = sel_features_[sel_features_[:,"MeasMass"] .<= (feature["MeasMass"]+10),:]
    end

    sel_features = feature_list_clean(sel_features_,feature,add_m)


    if length(sel_features[:,:Rt]) <=1

        p_n_ind = feature.Nr
        ind_parent = findfirst(x -> x == p_n_ind[1], feature_list.Nr)
        ind_ms1_comp = []
        #ind_comp_,add,mass = adduct_find(p_n_ind,sel_features_cor,mode,mass_win_per,add_m)
        mass = NaN
        return (ind_parent,ind_ms1_comp,mass)

    end


    corr_mat=corr_mat_ms1(chrom,sel_features,feature)
    r_ind=corr_check(corr_mat,r_thresh)

    if length(r_ind) > 0
        sel_features_cor = sel_features[r_ind,:] #[abs.(r_ind) .>= r_thresh],:]
    elseif any(sel_features.Nr .== feature.Nr)
        sel_features_cor = sel_features[sel_features.Nr .== feature.Nr,:]
    else
        sel_features_cor = deepcopy(feature)
    end

    ind_comp=zeros(size(sel_features_cor,1),3)

    if length(sel_features_cor.Nr) >=2 && minimum(abs.(sel_features_cor.Nr .- feature.Nr))==0

        pr=feature.Nr
        ind_comp[:,1],add,mass = adduct_find(pr,sel_features_cor,mode,mass_win_per,add_m)
        ind_comp[:,2]=iso_find(pr,sel_features_cor,delta_mass,ru_m,EMDmodel)
        if Ctype == "GC"
            #all remaining features are considered fragments
            ind_comp[:,3] = vec(all(ind_comp[:,1:2] .== 0,dims = 2))
        else
            ind_comp[:,3]=in_source_frag_find(pr,sel_features_cor,NL,mass_win_per)
        end

        p_n_ind = sel_features_cor.Nr[findall(x -> x == 3, sum(ind_comp,dims=2))]

        if length(p_n_ind)>1
            ind_parent = feature.Nr
            ind_parent = findfirst(x -> x == ind_parent, feature_list.Nr)
        elseif length(p_n_ind) == 1
            ind_parent = findall(x -> x == p_n_ind[1], feature_list.Nr)
        else
            ind_parent = feature.Nr
            ind_parent = findfirst(x -> x == ind_parent, feature_list.Nr)
        end




        if length(sel_features_cor.Nr[findall(x -> 0 < x < 3, sum(ind_comp,dims=2))]) > 0

            c_n_ind = sel_features_cor.Nr[findall(x -> 0 < x < 3, sum(ind_comp,dims=2))]
            ind_ms1_comp = Array{Int64}(undef,length(c_n_ind))
            # l=1
            for l=1:length(ind_ms1_comp)
                ind_ms1_comp[l] = findall(x ->  x == c_n_ind[l], feature_list.Nr)[1]
            end
        else
            ind_ms1_comp =[]

        end
    else
        p_n_ind = feature.Nr
        ind_parent = findall(x -> x == p_n_ind[1], feature_list.Nr)
        ind_ms1_comp=[]
        #ind_comp_,add,mass = adduct_find(p_n_ind,sel_features_cor,mode,mass_win_per,add_m)
        mass = NaN
    end

    return (ind_parent,ind_ms1_comp,mass)

end # function

##############################################################################
# removing aducts, iso, frag



function rm_ms1!(ind_parent,ind_ms1_comp,mass,decon_tab,feature_list)
    #convert feature num to index
    # ind_parent = findfirst(feature_list.Nr .== ind_parent)
    # for m = 1:length(ind_ms1_comp)
    #     # println(m)
    #     ind_ms1_comp[m] = findfirst(feature_list.Nr .== ind_ms1_comp[m])
    # end
    # try
    #     decon_tab[ind_parent,1:size(feature_list,2)] = Vector(feature_list[ind_parent,:])
    # catch
    if typeof(ind_parent) == Int64
        ind_parent = [ind_parent]
    end
    decon_tab[ind_parent,1:size(feature_list,2)] = Matrix(feature_list[ind_parent,:])
    # end


    if length(ind_ms1_comp)>1
        decon_tab[ind_parent[1],15]=1.0
        decon_tab[ind_parent[1],16]=mass
        decon_tab[ind_parent[1],size(feature_list,2)+3]=string(feature_list.MeasMass[ind_ms1_comp])
        decon_tab[ind_parent[1],size(feature_list,2)+4]=string(feature_list.Int[ind_ms1_comp])

        feature_list[ind_ms1_comp,:] .= 0.0
        decon_tab[ind_ms1_comp,:] .= 0.0
        feature_list[ind_parent,:] .= 0.0

    elseif (length(ind_ms1_comp)) ==1 && (ind_ms1_comp == ind_parent)
        decon_tab[ind_parent[1],15]=1.0
        decon_tab[ind_parent[1],16]=mass
        decon_tab[ind_parent[1],size(feature_list,2)+3] = 0
        decon_tab[ind_parent[1],size(feature_list,2)+4] = 0

        feature_list[ind_ms1_comp,:] .= 0.0

    elseif (length(ind_ms1_comp) == 1) && (ind_ms1_comp != ind_parent)
        decon_tab[ind_parent[1],15]=1.0
        decon_tab[ind_parent[1],16]=mass
        decon_tab[ind_parent[1],size(feature_list,2)+3]=string(feature_list.MeasMass[ind_ms1_comp])
        decon_tab[ind_parent[1],size(feature_list,2)+4]=string(feature_list.Int[ind_ms1_comp])

        feature_list[ind_ms1_comp,:] .= 0.0
        decon_tab[ind_ms1_comp,:] .= 0.0
        feature_list[ind_parent,:] .= 0.0

    elseif (length(ind_ms1_comp) == 0) && (length(ind_parent) > 0)
        decon_tab[ind_parent[1],15]=0
        decon_tab[ind_parent[1],16] = mass
        decon_tab[ind_parent[1],size(feature_list,2)+3]=0
        decon_tab[ind_parent[1],size(feature_list,2)+4]=0

    else
        decon_tab[ind_parent[1],15]=0
        decon_tab[ind_parent[1],16]=NaN
        decon_tab[ind_parent[1],size(feature_list,2)+3]=0
        decon_tab[ind_parent[1],size(feature_list,2)+4]=0
    end

    return(decon_tab,feature_list)

end # function

# rm_ms1!(ind_parent,ind_ms1_comp,mass,decon_tab,feature_list)

################################
# Signal selection SWATH

function sig_select_SWATH(chrom,timeWin,ind,massWin)

    MS1val=chrom["MS1"]["Mz_values"][timeWin[1]:timeWin[2],:]
    MS1int=chrom["MS1"]["Mz_intensity"][timeWin[1]:timeWin[2],:]

    ms_t = (massWin[2] - massWin[1])
    dm = abs.(MS1val .- mean(massWin))
    dm[dm .> ms_t] .=NaN
    dm[dm .<= ms_t] .=1
    dm[isnan.(dm) .== 1] .=0

    ##
    # !!!check...
    ms1val=zeros(size(MS1val,1),Int(maximum(sum(dm,dims=2))));
    ms1int=zeros(size(MS1val,1),Int(maximum(sum(dm,dims=2))));


    for i=1:size(MS1val,1)
        tv1=MS1val[i,dm[i,:] .==1]
        if length(tv1)>0
            ms1val[i,1:length(tv1)]=tv1
            ms1int[i,1:length(tv1)]=MS1int[i,dm[i,:] .==1]

        end

    end

    ##


    c=findall(x -> x == chrom["MS2"]["PrecursorIon"][ind],chrom["MS2"]["PrecursorIon"])


    MS2val=chrom["MS2"]["Mz_values"][c,:];
    MS2int=chrom["MS2"]["Mz_intensity"][c,:];
    Rt2 = chrom["MS2"]["Rt"][c];

    p1 = chrom["MS1"]["Rt"][timeWin[1]]
    p2 = chrom["MS1"]["Rt"][timeWin[2]]

    s2_s = findfirst(x ->  x >= p1,Rt2)
    s2_e = findlast(x ->  x <= p2,Rt2)

    if s2_e + 1 >=size(MS2int,1)
        ms2val = MS2val[s2_s-1:end,:]
        ms2int = MS2int[s2_s-1:end,:]
    elseif s2_s - 1 <= 0
        ms2val=MS2val[1:s2_e + 1,:]
        ms2int=MS2int[1:s2_e + 1,:]

    else
        ms2val=MS2val[s2_s:s2_e,:]
        ms2int=MS2int[s2_s:s2_e,:]
    end

    return(ms1val,ms1int,ms2val,ms2int)

end



################################
# Signal selection DIA


function sig_select_DIA(chrom,timeWin,massWin)

    MS1val=chrom["MS1"]["Mz_values"][timeWin[1]:timeWin[2],:]
    MS1int=chrom["MS1"]["Mz_intensity"][timeWin[1]:timeWin[2],:]

    ##

    ms_t = (massWin[2] - massWin[1])
    dm = abs.(MS1val .- mean(massWin))
    dm[dm .> ms_t] .=NaN
    dm[dm .<= ms_t] .=1
    dm[isnan.(dm) .== 1] .=0

    ##

    ms1val=zeros(size(MS1val,1),Int(maximum(sum(dm,dims=2))));
    ms1int=zeros(size(MS1val,1),Int(maximum(sum(dm,dims=2))));

    for i=1:size(MS1val,1)
        tv1=MS1val[i,dm[i,:] .==1]
        if length(tv1)>0
            ms1val[i,1:length(tv1)]=tv1
            ms1int[i,1:length(tv1)]=MS1int[i,dm[i,:] .==1]

        end

    end

    t1_s = chrom["MS1"]["Rt"][timeWin[1]]
    t1_e = chrom["MS1"]["Rt"][timeWin[2]]

    if findlast(x ->  x < t1_e, chrom["MS2"]["Rt"])+1 < length(chrom["MS2"]["Rt"])
        m = 1
    else
        m=0
    end



    s2s = findfirst(x ->  x > t1_s, chrom["MS2"]["Rt"])
    s2e = findlast(x ->  x < t1_e, chrom["MS2"]["Rt"])+m


    ms2val=chrom["MS2"]["Mz_values"][s2s:s2e,:]
    ms2int=chrom["MS2"]["Mz_intensity"][s2s:s2e,:]

    return(ms1val,ms1int,ms2val,ms2int)

end


################################
# Signal selection DIA mc


function sig_select_DIA_cl(chrom,timeWin,massWin)

    MS1val=chrom["MS1"]["Mz_values"][timeWin[1]:timeWin[2],:]
    MS1int=chrom["MS1"]["Mz_intensity"][timeWin[1]:timeWin[2],:]

    ##

    ms_t = (massWin[2] - massWin[1])
    dm = abs.(MS1val .- mean(massWin))
    dm[dm .> ms_t] .=NaN
    dm[dm .<= ms_t] .=1
    dm[isnan.(dm) .== 1] .=0

    ##

    ms1val=zeros(size(MS1val,1),Int(maximum(sum(dm,dims=2))));
    ms1int=zeros(size(MS1val,1),Int(maximum(sum(dm,dims=2))));

    for i=1:size(MS1val,1)
        tv1=MS1val[i,dm[i,:] .==1]
        if length(tv1)>0
            ms1val[i,1:length(tv1)]=tv1
            ms1int[i,1:length(tv1)]=MS1int[i,dm[i,:] .==1]

        end

    end

    t1_s = chrom["MS1"]["Rt"][timeWin[1]]
    t1_e = chrom["MS1"]["Rt"][timeWin[2]]

    if findlast(x ->  x < t1_e, chrom["MS2"]["Rt"])+1 < length(chrom["MS2"]["Rt"])
        m = 1
    else
        m=0
    end



    s2s = findfirst(x ->  x > t1_s, chrom["MS2"]["Rt"])
    s2e = findlast(x ->  x < t1_e, chrom["MS2"]["Rt"])+m


    ms2val_t=chrom["MS2"]["Mz_values"][s2s:s2e,:]
    ms2int_t=chrom["MS2"]["Mz_intensity"][s2s:s2e,:]

    c=round(size(ms2val_t,1)/size(ms1val,1))
    ms2val=Array{Any}(undef,Int(c))
    ms2int=Array{Any}(undef,Int(c))

    for i=1:Int(c)
        ms2val[i] = ms2val_t[i:Int(c):size(ms2val_t,1),:]
        ms2int[i] = ms2int_t[i:Int(c):size(ms2val_t,1),:]
    end

    ####


    return(ms1val,ms1int,ms2val,ms2int)

end


#########################################################################
# XIC generator

function xic_gen_ms2!(mass2,mass_tol,ms2val,ms2int)

    ms2_val_sel=zeros(size(ms2int,1),1)
    ms2_int_sel=zeros(size(ms2int,1),1)
    ms2_int_out=copy(ms2int)

    for i=1:size(ms2int,1)
        #println(i)
        tv1 = ms2val[i,findall(x -> mass2+mass_tol > x >= mass2-mass_tol,ms2val[i,:])]
        if length(tv1)>0
            #println(i)
            ms2_val_sel[i,1]=median(tv1)
            ms2_int_sel[i,1]=maximum(ms2int[i,findall(x -> mass2+mass_tol >= x >= mass2-mass_tol,ms2val[i,:])])
            ms2_int_out[i,findall(x -> mass2+mass_tol >= x >= mass2-mass_tol,ms2val[i,:])].=0
        end


    end
    return ms2_val_sel,ms2_int_sel,ms2_int_out

end

########################################################################

function xic_ms1(loc,ms1int,ms1val,mass_tol)

    xic = zeros(size(ms1int,1))

    for i =1:length(xic)
        if minimum(abs.(ms1val[loc] .- ms1val[i,:])) <= mass_tol
            tv = abs.(ms1val[loc] .- ms1val[i,:])
            xic[i] = maximum(ms1int[i,tv .<=mass_tol])
        end

    end
    return xic

end


########################################################################
# Round by

# x = 1.0075
# r = 0.005

function roundby(x,r)
    return round.(round.(x ./ r) .* r,digits=4)

end




#########################################################################
# Correlation matrix generator

function corrlation_mat_gen(ms1int,ms1val,ms2int,ms2val,min_int,mass_tol,NLmod,ret_tol)

    val,loc1=findmax(ms1int)
    xic= xic_ms1(loc1,ms1int,ms1val,mass_tol)

    if length(xic[xic .>0]) <=1 || length(ms2val[ms2val .>0]) ==0
        corr_mat = []
        frags = []
        return(corr_mat,frags)

    end

    min_ms = minimum(ms2val[ms2val .>0])

    tr_m = mean(ms1val[ms1val .>0]) .- vcat(0,unique(roundby(NLmod["mass"][NLmod["prob"] .>=0.05],mass_tol)))
    tr_m_s = tr_m[tr_m .>= min_ms]

    #MS2 intensity check for relevant CNLs !!!
    temp = vec(ms2val)[vec(ms2int).>= min_int]
    # temp = temp[temp[:,2] .>= min_int, 1]
    temp = unique(round.(temp,digits = 3))
    for t = 1:length(tr_m_s)
        if !any(abs.(tr_m_s[t].-temp) .< mass_tol)
            tr_m_s[t] = 0
        end
    end
    tr_m_s = tr_m_s[tr_m_s .!= 0]
    # println(length(tr_m_s))

    if size(ms2int,1) >= size(ms1int,1)

        corr_mat_t=zeros(size(ms2int,1),length(tr_m_s))
        xic = vcat(xic,zeros(size(ms2int,1)-size(ms1int,1)))
    else
        corr_mat_t=zeros(size(ms1int,1),length(tr_m_s))
    end

    mz_v = deepcopy(ms2val)

    for i=1:length(tr_m_s)
        xic2,m_rep,mz_v = xic_extract(mz_v,ms2int,tr_m_s[i],mass_tol)
        if length(xic2[xic2 .>0]) >3 && maximum(xic2) >= min_int && abs(loc1[1] - argmax(xic2)[1]) <= ret_tol
            corr_mat_t[1:length(xic2),i] = sma(xic2,3)
            tr_m_s[i] = m_rep
        else
            #println(i)
            tr_m_s[i] = 0
        end


    end

    if length(tr_m_s[tr_m_s .>0]) == 0
        corr_mat = []
        frags = []
        return(corr_mat,frags)
    end


    corr_mat =hcat(sma(xic,3),corr_mat_t[:,tr_m_s .>0])
    frags = vcat(mean(ms1val[ms1val .> 0]),tr_m_s[tr_m_s .>0])

    return(corr_mat,frags)
end

###################################################################
# Apex check

function apex_check(corr_mat,ret_tol)
    vals,locs=findmax(corr_mat,dims=1)
    corr_mat_cleaned=copy(corr_mat)
    target_ret=locs[1][1]
    for i=1:length(vals)
        tv1=corr_mat_cleaned[:,i]
        #corr_mat_cleaned[:,i]=corr_mat[:,i]
        if abs(locs[i][1]-target_ret)>ret_tol
            corr_mat_cleaned[:,i] .=0

        elseif length(tv1[tv1 .>0]) <2
            corr_mat_cleaned[:,i] .=0
        end


    end

    return corr_mat_cleaned
end

#
####################################################################
# fragement extractor

function frag_extract(corr_mat_cleaned,frags,r_thresh)

    r_ind=corr_check(corr_mat_cleaned,r_thresh)
    if !isempty(r_ind)
        frag_mz=frags[r_ind .+ 1]

        sel_corr_mat=corr_mat_cleaned[:,r_ind .+ 1]
        frag_int=maximum(sel_corr_mat,dims=1)
    else
        frag_mz = []
        frag_int = []
    end

    return(frag_mz,frag_int)
end




###############################################################################
# Function for finding MS2 adducts
#

function adduct_find_MS2(mass,frag_mz,add_m,mass_tol)


    dmass=frag_mz .- mass

    x=zeros(length(dmass),1)

    for i=1:length(dmass)
        #println(i)
        err=abs.(abs(dmass[i]) .- add_m)
        tv=findall(x -> x <= mass_tol[1]/2, err)
        if length(tv)>0
            x[i]=1
        end
    end

    return x


end # function


###############################################################################
# Function for finding MS2 isotopes
#

function iso_find_ms2(mass,frag_mz,ru_m,delta_mass)

    x=zeros(size(frag_mz,1),1)
    kmd_p=KMD_calc_ext(mass,ru_m["ru_m"])

    for i=1:size(x,1)
        if frag_mz[i] > mass && frag_mz[i] - mass <= 5*1.0033
            kmd_temp=KMD_calc_ext(frag_mz[i],ru_m["ru_m"])
            if round(median(abs.(kmd_p[1] .- kmd_temp[1]));digits=3)<= 2*delta_mass
                x[i]=1
            end
        end

    end

    return x
end # function

###############################################################################
# Function for finding MS2 fragments
#

function frag_find_MS2(mass,frag_mz,mass_tol,NL)

    x=zeros(size(frag_mz,1),1)

    n_ls=mass .- frag_mz

    for i=1:size(x,1)
        if abs(n_ls[i]) <= mass_tol/2
            x[i]=1

        elseif n_ls[i] > 0 && any(abs.(n_ls[i] .- NL["mass"]) .<= mass_tol/2) &&
            NL["prob"][argmin(abs.(n_ls[i] .- NL["mass"]))]>= 0.05
            x[i]=1

        end
    end

    return x
end # function


###############################################################################
# Function for filtering MS2 frags
#

function ms2filter(mass,mass2rep,frag_mz,frag_int,mass_tol,NL,ru_m,delta_mass,add_m,Ctype)

    compInd=zeros(length(frag_mz),3)
    ad_ms2=0
    if length(frag_mz)>0
        compInd[:,1]=adduct_find_MS2(mass2rep,frag_mz,add_m,mass_tol)
        compInd[:,2] = iso_find_ms2(mass,frag_mz,ru_m,delta_mass)
        if Ctype == "GC"
            #all remaining features are considered fragments
            compInd[:,3] = vec(all(compInd[:,1:2] .== 0,dims = 2))
        else
            compInd[:,3]=frag_find_MS2(mass,frag_mz,mass_tol,NL)
        end
    end

    frag_mz_sel=frag_mz[findall(x -> x > 0,sum(compInd, dims=2))]
    frag_int_sel=frag_int[1,findall(x -> x > 0,sum(compInd, dims=2))]

    if sum(compInd[:,1])>0 || sum([compInd[:,1];compInd[:,3]])>0
        ad_ms2=1

    end

    return(frag_mz_sel,frag_int_sel,ad_ms2)

end # function


###############################################################################
# warper function decon single feature for SWATH internal feature list
# parameters


function feature_decon_SWATH_singlefeature_internal(chrom,pre_mz,
    r_thresh,min_int,feature,mass_win_per,ret_win_per,NL)

    SWATH_n=length(pre_mz)
    massWin=[feature.MinMass,feature.MaxMass]
    mass_tol=mass_win_per*(feature.MaxMass - feature.MinMass)
    mass=feature.MeasMass

    if mass >= minimum(chrom["MS1"]["Mz_values"]) && mass <= maximum(chrom["MS1"]["Mz_values"])


        ind=argmin(abs.(mass .- pre_mz))
        # timeWin=[floor(Int,feature.ScanNum - feature.ScanInPeak),ceil(Int,feature.ScanNum + feature.ScanInPeak)]
        timeWin=[floor(Int,feature.ScanNum - feature.ScanInPeak),ceil(Int,feature.ScanNum + feature.ScanInPeak)]

        if timeWin[1]<=0
            timeWin[1]=1
        elseif timeWin[2]>= minimum([size(chrom["MS1"]["Mz_intensity"],1),
            length(chrom["MS2"]["PrecursorIon"][chrom["MS2"]["PrecursorIon"][:] .== chrom["MS2"]["PrecursorIon"][1]])])

            timeWin[2]= minimum([size(chrom["MS1"]["Mz_intensity"],1),
            length(chrom["MS2"]["PrecursorIon"][chrom["MS2"]["PrecursorIon"][:] .== chrom["MS2"]["PrecursorIon"][1]])])

        end

        ret_tol=ceil(ret_win_per*feature.ScanInPeak/2)

        if  ret_tol >3
            ret_tol = 3
        elseif  ret_tol <=1
            ret_tol =3
        end

        ms1val,ms1int,ms2val,ms2int=sig_select_SWATH(chrom,timeWin,ind,massWin);

        if length(ms1int[ms1int .>0]) == 0
            frag_mz=[]
            frag_int=[]
            return frag_mz,frag_int,mass_tol
        end

        corr_mat,frags = corrlation_mat_gen(ms1int,ms1val,ms2int,ms2val,min_int,mass_tol,NL,ret_tol)

        if size(corr_mat,1) > 0
            corr_mat_cleaned=corr_mat
        else
            corr_mat_cleaned = []
        end

        if size(corr_mat_cleaned,2)>1
            frag_mz,frag_int=frag_extract(corr_mat_cleaned,frags,r_thresh)
        else
            frag_mz=[]
            frag_int=[]
        end


    else
        println("This feature was not within the defined SWATH windows.")
        frag_mz=[]
        frag_int=[]

    end


    return frag_mz,frag_int,mass_tol


end

###############################################################################
# warper function decon single feature for DIA internal feature list
#

function feature_decon_DIA_singlefeature_internal(chrom,r_thresh,
    min_int,feature,mass_win_per,ret_win_per,NL)


    massWin=[feature.MinMass,feature.MaxMass]
    mass_tol=mass_win_per*(feature.MaxMass - feature.MinMass)
    mass=feature.MeasMass


    timeWin=[floor(Int,feature.ScanNum - feature.ScanInPeak),ceil(Int,feature.ScanNum + feature.ScanInPeak)]

    if timeWin[1]<=0
        timeWin[1]=1
    elseif timeWin[2]>= minimum([size(chrom["MS1"]["Mz_intensity"],1),size(chrom["MS2"]["Mz_intensity"],1)])

        timeWin[2]=minimum([size(chrom["MS1"]["Mz_intensity"],1),size(chrom["MS2"]["Mz_intensity"],1)])

    end



    ret_tol=floor(ret_win_per*feature.ScanInPeak/2)

    ms1val,ms1int,ms2val,ms2int=sig_select_DIA(chrom,timeWin,massWin)
    corr_mat,frags=corrlation_mat_gen(ms1int,ms1val,ms2int,ms2val,min_int,mass_tol,NL,ret_tol)

    frag_mz,frag_int=frag_extract(corr_mat,frags,r_thresh)




    return frag_mz,frag_int,mass_tol


end



###############################################################################
# wraper function decon single file for DIA internal feature list
#

function comp_DIA_ESI(chrom,path2features,mass_win_per,
    ret_win_per,r_thresh,mode,delta_mass,min_int,Ctype,isoModel,fragModel)

    max_mass=maximum(chrom["MS1"]["Mz_values"][:])
    pathAUX = "C:\\Users\\dherwer\\OneDrive - UvA\\Algorithm repositories\\Bitbucket\\compcreate.jl\\src"
    path2aux=joinpath(pathAUX,"AUX_data.jld2")
    try
        mm=pathof(CompCreate)
        path2aux=joinpath(mm[1:end-13],"AUX_data.jld2")
    catch
    end
    #println(path2aux)
    a_d=load(path2aux,"AUX_data") # Auxilary data

    # a_d=load("AUX_data.jld2","AUX_data"); # Auxilary data


    if mode == "POSITIVE"
        add_m=a_d["Ad"]["pos"]

    elseif mode == "NEGATIVE"

        add_m=a_d["Ad"]["neg"]
    end

    ru_m=a_d["Iso"]

    if fragModel == "old"
        NL=a_d["Frag"]
        println("Running with old fragment model")
    else
        NL = a_d["FragCNL"]
        println("Running with new fragment (CNL) model")
    end

    if isoModel == "old"
        EMDmodel = []
        println("Running with old isotope model")
    else
        path = pathof(CompCreate)[1:end-13]
        EMDmodel = BSON.load(joinpath(path,"ProbabilityDistribution.bson"))
        println("Running with new EMD isotope model")
    end

    feature_list = DataFrame(CSV.File(path2features))
    comp_order = sortperm(feature_list[:,"Int"], rev = true)
    decon_tab=Matrix(undef,size(feature_list,1),size(feature_list,2)+6);

    for i in ProgressBar(comp_order)
    #@time for i = 1 : size(feature_list,1)
    #@showprogress 1 "Computing..." for i in 1:size(feature_list,1)
		# try
        #println(i)
        #sleep(0.1)
        if feature_list.ScanNum[i]>0 && feature_list.MeasMass[i]<=max_mass

            feature=deepcopy(feature_list[i,:])

            ind_parent,ind_ms1_comp,mass = ms1_comp_finder(feature,feature_list,chrom,ret_win_per,r_thresh,
                delta_mass,add_m,ru_m,NL,mode,mass_win_per,Ctype,EMDmodel)



            decon_tab,feature_list = rm_ms1!(ind_parent,ind_ms1_comp,mass,decon_tab,feature_list);


            massWin=[feature.MinMass,feature.MaxMass]
            mass_tol=mass_win_per*(feature.MaxMass-feature.MinMass)
            mass=feature.MeasMass
            #feature=feature_list[i,:]
            #println(i)
            timeWin=[floor(Int,feature.ScanNum - feature.ScanInPeak),ceil(Int,feature.ScanNum + feature.ScanInPeak)]

            if length(timeWin[timeWin .> 0]) == 0

                decon_tab[i,:] .= 0
                continue
            end

            if timeWin[1]<=0 && timeWin[2]>0
                timeWin[1]=1
            elseif timeWin[2]>= size(chrom["MS1"]["Mz_intensity"],1)
                timeWin[2]=size(chrom["MS1"]["Mz_intensity"],1)
            end



            ret_tol=floor(ret_win_per*feature.ScanInPeak)

            ms1val,ms1int,ms2val,ms2int=sig_select_DIA(chrom,timeWin,massWin)

            corr_mat,frags=corrlation_mat_gen(ms1int,ms1val,ms2int,ms2val,min_int,mass_tol,NL,ret_tol)

            if !isempty(frags)
                frag_mz,frag_int=frag_extract(corr_mat,frags,r_thresh)
            else
                frag_mz = []
                frag_int = []
            end


            if mode == "POSITIVE"
                mass2rep=feature_list.MeasMass[i] - 1.007825
                mass=feature_list.MeasMass[i]
            elseif mode == "NEGATIVE"
                mass2rep=feature_list.MeasMass[i] + 1.007825
                mass=feature_list.MeasMass[i]
            end

            if !isempty(frag_mz) >0

                frag_mz_sel,frag_int_sel,ad_ms2=ms2filter(mass,mass2rep,frag_mz,frag_int,mass_tol,NL,ru_m,delta_mass,add_m,Ctype)
            else
                frag_mz_sel = []
            end




            if length(frag_mz_sel)>0 && ad_ms2 ==1
                decon_tab[ind_parent[1],15] = 1.0
                decon_tab[ind_parent[1],16] = mass2rep
                decon_tab[ind_parent[1],19] = string(frag_mz_sel[:])
                decon_tab[ind_parent[1],20] = string(frag_int_sel[:])
                #decon_tab[ind_ms1_comp[1],:] .= 0
            elseif length(frag_mz_sel)>0 && ad_ms2 ==0
                decon_tab[ind_parent[1],19] = string(frag_mz_sel[:])
                decon_tab[ind_parent[1],20] = string(frag_int_sel[:])

            else
                decon_tab[ind_parent[1],19:end] .=0
                #decon_tab[ind_parent[1],20]=0


            end

        else
            decon_tab[i,:] .= 0
            #println(i)

        end

		# catch e
		# 	catch_error(e,i)
		# end

    end


    # decon_tab[46,:]

    table=DataFrame(decon_tab,[:Nr,:ScanNum,:ScanInPeak,:Rt,:RtStart,:RtEnd,:MinInPeak,
    :MeasMass,:MinMass,:MaxMass,:Area,:Int,:FeatPurity,:MediRes,:Parent,:AccuMass,:MS1Comp,:MS1CompInt,:MS2Comp,:MS2CompInt])
    sort!(table,[:Nr])

    final_table=table[table[!,:Int] .> 0,:]
    final_table.MS1CompInt[final_table.MS1CompInt.==0] .= "[]"
    final_table.MS1Comp[final_table.MS1Comp.==0] .= "[]"
    final_table.MS2CompInt[final_table.MS2CompInt.==0] .= "[]"
    final_table.MS2Comp[final_table.MS2Comp.==0] .= "[]"
    # final_table.Nr=1:size(final_table,1)
    m=prod(split(path2features,".")[1:end-1] .* ".")[1:end-1]
    output=string(m,"_comp.csv")
    CSV.write(output,final_table)
    println("The final report has been saved in the output path!")
    return(final_table)


end


###############################################################################
# wraper function decon single file for DIA internal feature list
#

function comp_DDA_ESI(chrom,path2features,mass_win_per,
    ret_win_per,r_thresh,mode,delta_mass,min_int,Ctype,isoModel,fragModel)

    max_mass=maximum(chrom["MS1"]["Mz_values"][:])
    pathAUX = "C:\\Users\\dherwer\\OneDrive - UvA\\Algorithm repositories\\Bitbucket\\compcreate.jl\\src"
    path2aux=joinpath(pathAUX,"AUX_data.jld2")
    try
        mm=pathof(CompCreate)
        path2aux=joinpath(mm[1:end-13],"AUX_data.jld2")
    catch
    end
    #println(path2aux)
    a_d=load(path2aux,"AUX_data") # Auxilary data

    # a_d=load("/Users/saersamanipour/Desktop/dev/pkgs/CompCreate/src/AUX_data.jld2","AUX_data"); # Auxilary data



    # JLD2.jldopen("/Users/saersamanipour/Desktop/dev/pkgs/CompCreate/AUX_data.jld2", "w") do file
    #    write(file, "AUX_data", a_d)  # alternatively, say "@write file A"
    # end
    pre_mz_1 = Float64.(chrom["MS2"]["PrecursorIon"])

    if mode == "POSITIVE"
        add_m=a_d["Ad"]["pos"]

    elseif mode == "NEGATIVE"

        add_m=a_d["Ad"]["neg"]
    end

    ru_m=a_d["Iso"]

    if fragModel == "old"
        NL=a_d["Frag"]
        println("Running with old fragment model")
    else
        NL = a_d["FragCNL"]
        println("Running with new fragment (CNL) model")
    end

    if isoModel == "old"
        EMDmodel = []
        println("Running with old isotope model")
    else
        path = pathof(CompCreate)[1:end-13]
        EMDmodel = BSON.load(joinpath(path,"ProbabilityDistribution.bson"))
        println("Running with new EMD isotope model")
    end

    feature_list = DataFrame(CSV.File(path2features))
    comp_order = sortperm(feature_list[:,"Int"], rev = true)
    decon_tab=Matrix(undef,size(feature_list,1),size(feature_list,2)+6);
    tc_e = length(chrom["MS1"]["Rt"])
    # i=31
    for i in ProgressBar(comp_order)
    #@time for i = 1:3136

    #@showprogress 1 "Computing..." for i in 1:size(feature_list,1)
		# try
			if feature_list.ScanNum[i]>0 && feature_list.MeasMass[i]<=max_mass

				feature=deepcopy(feature_list[i,:])


				mass_tol=mass_win_per*(feature_list.MaxMass[i]-feature_list.MinMass[i])

                # get MS2 scans in peak tr window
				Rt_i = feature["RtStart"]#chrom["MS1"]["Rt"][Int(feature_list.ScanNum[i])]
                Rt_e = feature["RtEnd"]
				ms2_ind = findall(x -> Rt_i <= x <= Rt_e ,chrom["MS2"]["Rt"])
				d_m =  abs.(feature_list.MeasMass[i] .- pre_mz_1[ms2_ind])
				res = feature_list.MediRes[i]

                #check if there are any ms2 fragments
                mz_val_cent = [0]
				mz_int_cent = [0]
                if any(d_m .<= 2*mass_tol)
                    #println("ciao")
                    mz_val = chrom["MS2"]["Mz_values"][ms2_ind[d_m .<= 2*mass_tol],:]
                    mz_int = chrom["MS2"]["Mz_intensity"][ms2_ind[d_m .<= 2*mass_tol],:]

                    for j =1:length(d_m[d_m .<= 2*mass_tol])
                        #println(j)
                        mz_val_cent_,mz_int_cent_,dm_c = peak_finder(mz_val[j,:],mz_int[j,:],res,0.02,min_int,r_thresh,1.1,ms2_ind[d_m .<= 2*mass_tol][j])
                        mz_val_cent = vcat(mz_val_cent,mz_val_cent_)
                        mz_int_cent = vcat(mz_int_cent,mz_int_cent_)
                    end
                    #keep unique on 3 decimals with highest int 
                    mz_val_f = []
                    mz_int_f = []
                    mz_val_cent = round.(mz_val_cent, digits = 3)
                    for u in unique(mz_val_cent)
                        ind = findall(u .== mz_val_cent)
                        ind_m = argmax(mz_int_cent[ind])
                        mz_val_f = [mz_val_f; mz_val_cent[ind[ind_m]]]
                        mz_int_f = [mz_int_f; mz_int_cent[ind[ind_m]]]
                    end
                    mz_val_cent = mz_val_f
                    mz_int_cent = mz_int_f
                end
                
                ind_parent,ind_ms1_comp,mass = ms1_comp_finder(feature,feature_list,chrom,ret_win_per,r_thresh,
                    delta_mass,add_m,ru_m,NL,mode,mass_win_per,Ctype,EMDmodel)
                #check if ms2 information has been found
                if length(mz_val_cent)>1 
                    # check if in-source fragments are potential precursor -> remove from removal list
                    if !isempty(ind_ms1_comp)
                        ind_c = findall(feature_list[ind_ms1_comp,"MeasMass"] .< mass)
                        if !isempty(ind_c)
                            for c in ind_c
                                #check if its a potential precursor
				                tms2_ind = findall(x -> feature_list[ind_ms1_comp[c],"RtStart"] <= x <= feature_list[ind_ms1_comp[c],"RtEnd"] ,chrom["MS2"]["Rt"])
                                if any(abs.(feature_list.MeasMass[ind_ms1_comp[c]] .- pre_mz_1[tms2_ind]).<mass_win_per*(feature_list.MaxMass[ind_ms1_comp[c]]-feature_list.MinMass[ind_ms1_comp[c]]))
                                    ind_ms1_comp[c] = 0
                                end
                            end
                            # remove potential precursors
                            ind_ms1_comp = ind_ms1_comp[ind_ms1_comp .!= 0]
                            if isempty(ind_ms1_comp)
                                ind_ms1_comp = []
                            end
                        end
                    end
                else
                    ind_ms1_comp = []
                end
                decon_tab,feature_list = rm_ms1!(ind_parent,ind_ms1_comp,mass,decon_tab,feature_list);

				if length(mz_val_cent)>0 && mz_val_cent !=0 && isnan(mass) == 0
					decon_tab[ind_parent[1],15] =1.0
					decon_tab[ind_parent[1],16] = mass
					decon_tab[ind_parent[1],19] = string(mz_val_cent[2:end])
					decon_tab[ind_parent[1],20] = string(mz_int_cent[2:end])
					#decon_tab[ind_ms1_comp[1],:] .= 0

                    feature_list[ind_parent,:] .= 0.0

                elseif length(mz_val_cent)>0 && mz_val_cent !=0 && isnan(mass) == 1

                    if mode == "POSITIVE"
                        mass_1 = feature["MeasMass"] - 1.007276

                    elseif mode == "NEGATIVE"

                        mass_1 = feature["MeasMass"] + 1.007276
                    end

                    decon_tab[ind_parent[1],15] =1.0
					decon_tab[ind_parent[1],16] = mass_1
					decon_tab[ind_parent[1],19] = string(mz_val_cent[2:end])
					decon_tab[ind_parent[1],20] = string(mz_int_cent[2:end])

				else
					decon_tab[ind_parent[1],19:end] .=0
					#decon_tab[ind_parent[1],20]=0

				end

			else
				decon_tab[i,:] .= 0
				#println(i)

			end

		# catch e
		# 	catch_error(e,i)
		# end

    end


#    decon_tab[vec(all(Matrix(feature_list) .== 0, dims = 2)),:]


    table=DataFrame(decon_tab,[:Nr,:ScanNum,:ScanInPeak,:Rt,:RtStart,:RtEnd,:MinInPeak,
        :MeasMass,:MinMass,:MaxMass,:Area,:Int,:FeatPurity,:MediRes,:Parent,:AccuMass,:MS1Comp,:MS1CompInt,:MS2Comp,:MS2CompInt])
    sort!(table,[:Nr])
    final_table=table[table[!,:Int] .> 0,:]

    final_table.MS1CompInt[final_table.MS1CompInt.==0] .= "[]"
    final_table.MS1Comp[final_table.MS1Comp.==0] .= "[]"
    final_table.MS2CompInt[final_table.MS2CompInt.==0] .= "[]"
    final_table.MS2Comp[final_table.MS2Comp.==0] .= "[]"
    # final_table.Nr=1:size(final_table,1)
    m=prod(split(path2features,".")[1:end-1] .* ".")[1:end-1]
    output=string(m,"_comp.csv")
    CSV.write(output,final_table)
    println("The final report has been saved in the output path!")
    return(final_table)


end



###############################################################################
# wraper function decon single file for DIA internal feature list
#

function comp_DIA_ESI_mc(chrom,path2features,mass_win_per,
    ret_win_per,r_thresh,mode,delta_mass,min_int,Ctype,isoModel,fragModel)

    max_mass=maximum(chrom["MS1"]["Mz_values"][:])
    pathAUX = "C:\\Users\\dherwer\\OneDrive - UvA\\Algorithm repositories\\Bitbucket\\compcreate.jl\\src"
    path2aux=joinpath(pathAUX,"AUX_data.jld2")
    try
        mm=pathof(CompCreate)
        path2aux=joinpath(mm[1:end-13],"AUX_data.jld2")
    catch
    end
    #println(path2aux)
    a_d=load(path2aux,"AUX_data") # Auxilary data

    # a_d=load("AUX_data.jld2","AUX_data") # Auxilary data

    if mode == "POSITIVE"
        add_m=a_d["Ad"]["pos"]

    elseif mode == "NEGATIVE"

        add_m=a_d["Ad"]["neg"]
    end

    ru_m=a_d["Iso"]    
    
    if fragModel == "old"
        NL=a_d["Frag"]
        println("Running with old fragment model")
    else
        NL = a_d["FragCNL"]
        println("Running with new fragment (CNL) model")
    end

    if isoModel == "old"
        EMDmodel = []
        println("Running with old isotope model")
    else
        path = "C:\\Users\\dherwer\\OneDrive - UvA\\Algorithm repositories\\Bitbucket\\compcreate.jl\\src"
        try
            path = pathof(CompCreate)[1:end-13]
        catch
        end
        EMDmodel = BSON.load(joinpath(path,"ProbabilityDistribution.bson"))
        println("Running with new EMD isotope model")
    end

    feature_list = DataFrame(CSV.File(path2features))
    comp_order = sortperm(feature_list[:,"Int"], rev = true)
    decon_tab=Matrix(undef,size(feature_list,1),size(feature_list,2)+6);

    for i in ProgressBar(comp_order)
    #@showprogress 1 "Computing..." for i in 1:size(feature_list,1)
		# try
	#		sleep(0.1)

			if feature_list.ScanNum[i]>0 && feature_list.MeasMass[i]<=max_mass

				feature=deepcopy(feature_list[i,:])

				ind_parent,ind_ms1_comp,mass = ms1_comp_finder(feature,feature_list,chrom,ret_win_per,r_thresh,
					delta_mass,add_m,ru_m,NL,mode,mass_win_per,Ctype,EMDmodel)



				decon_tab,feature_list=rm_ms1!(ind_parent,ind_ms1_comp,mass,decon_tab,feature_list);


				massWin=[feature.MinMass,feature.MaxMass]
				mass_tol=mass_win_per*(feature.MaxMass-feature.MinMass)
				mass=feature.MeasMass
				#feature=feature_list[i,:]

				timeWin=[floor(Int,feature.ScanNum - feature.ScanInPeak),ceil(Int,feature.ScanNum + feature.ScanInPeak)]

				if timeWin[1]<=0
					timeWin[1]=1
				elseif timeWin[2]>= minimum([size(chrom["MS1"]["Mz_intensity"],1),size(chrom["MS2"]["Mz_intensity"],1)])
					timeWin[2]=minimum([size(chrom["MS1"]["Mz_intensity"],1),size(chrom["MS2"]["Mz_intensity"],1)])
				end



				ret_tol=floor(ret_win_per*feature.ScanInPeak/2)


				ms1val,ms1int,ms2val,ms2int = sig_select_DIA_cl(chrom,timeWin,massWin)

				frag_mz_sel_=Array{Any}(undef,length(ms2val))
				frag_int_sel_=Array{Any}(undef,length(ms2val))
				ad_ms2_=Array{Any}(undef,length(ms2val))
				#println(i)

				if mode == "POSITIVE"
					mass2rep=feature_list.MeasMass[i]- 1.007825
					mass=feature_list.MeasMass[i]
				elseif mode == "NEGATIVE"

					mass2rep=feature_list.MeasMass[i] + 1.007825
					mass=feature_list.MeasMass[i]

				end            # i=1
				#
				for j=1:length(ms2val)
					#println(j)
					corr_mat,frags=corrlation_mat_gen(ms1int,ms1val,ms2int[j],ms2val[j],min_int,mass_tol,NL,ret_tol)

					frag_mz,frag_int=frag_extract(corr_mat,frags,r_thresh)

                    if !isempty(frag_mz)
                        frag_mz_sel_[j],frag_int_sel_[j],ad_ms2_[j]=ms2filter(mass,mass2rep,frag_mz,
						    frag_int,mass_tol,NL,ru_m,delta_mass,add_m,Ctype)
                    else
                        frag_mz_sel_[j] = []
                        frag_int_sel_[j] = []
                        ad_ms2_[j] = 0
                    end

				end

				frag_mz_sel=zeros(1,length(ms2val))
				frag_int_sel=zeros(1,length(ms2val))
				for j=1:length(ms2val)
                    #println(j)
					if length(frag_mz_sel_[j][:])>0
						frag_mz_sel[j]=frag_mz_sel_[j][1]
						frag_int_sel[j]=frag_int_sel_[j][1]
					end
				end



				if length(sum(frag_mz_sel))>0 && sum(ad_ms2_) >=1
					decon_tab[ind_parent[1],15]=1.0
					decon_tab[ind_parent[1],16]=mass2rep
					decon_tab[ind_parent[1],19] = string(frag_mz_sel[frag_mz_sel[:] .>0])
					decon_tab[ind_parent[1],20] = string(frag_int_sel[frag_mz_sel[:] .>0])
				elseif length(sum(frag_mz_sel))>0 && sum(ad_ms2_) ==0
					decon_tab[ind_parent[1],19] = string(frag_mz_sel[frag_mz_sel[:] .>0])
					decon_tab[ind_parent[1],20] = string(frag_int_sel[frag_mz_sel[:] .>0])

				else
					decon_tab[ind_parent[1],19]=0
					decon_tab[ind_parent[1],20]=0

				end

			else
				decon_tab[i,:] .= 0

			end

        # catch e
        #     catch_error(e,i)
        # end

    end



    table=DataFrame(decon_tab,[:Nr,:ScanNum,:ScanInPeak,:Rt,:RtStart,:RtEnd,:MinInPeak,
    :MeasMass,:MinMass,:MaxMass,:Area,:Int,:FeatPurity,:MediRes,:Parent,:AccuMass,:MS1Comp,:MS1CompInt,:MS2Comp,:MS2CompInt])
    sort!(table,[:Nr])

    final_table=table[table[!,:Int] .> 0,:]

    final_table.MS1CompInt[final_table.MS1CompInt.==0] .= "[]"
    final_table.MS1Comp[final_table.MS1Comp.==0] .= "[]"
    final_table.MS2CompInt[final_table.MS2CompInt.==0] .= "[]"
    final_table.MS2Comp[final_table.MS2Comp.==0] .= "[]"
    # final_table.Nr=1:size(final_table,1)
    m=prod(split(path2features,".")[1:end-1] .* ".")[1:end-1]
    output=string(m,"_comp.csv")
    CSV.write(output,final_table)
    println("The final report has been saved in the output path!")
    return(final_table)


end



#############################################################################
# Wrapping function for componentization of SWATH

function comp_SWATH_ESI(chrom,path2features,mass_win_per,
    ret_win_per,r_thresh,mode,delta_mass,pre_mz,min_int,Ctype,isoModel,fragModel)

    max_mass=maximum(chrom["MS1"]["Mz_values"][:])
    pathAUX = "C:\\Users\\dherwer\\OneDrive - UvA\\Algorithm repositories\\Bitbucket\\compcreate.jl\\src"
    path2aux=joinpath(pathAUX,"AUX_data.jld2")
    try
        mm=pathof(CompCreate)
        path2aux=joinpath(mm[1:end-13],"AUX_data.jld2")
    catch
    end
    #println(path2aux)
    a_d=load(path2aux,"AUX_data") # Auxilary data
    # a_d=load("AUX_data.jld2","AUX_data") # Auxilary data
    #println(a_d)

    if mode == "POSITIVE"
        add_m=a_d["Ad"]["pos"]

    elseif mode == "NEGATIVE"

        add_m=a_d["Ad"]["neg"]
    end

    ru_m=a_d["Iso"]

    if fragModel == "old"
        NL=a_d["Frag"]
        println("Running with old fragment model")
    else
        NL = a_d["FragCNL"]
        println("Running with new fragment (CNL) model")
    end

    if isoModel == "old"
        EMDmodel = []
        println("Running with old isotope model")
    else
        path = pathof(CompCreate)[1:end-13]
        EMDmodel = BSON.load(joinpath(path,"ProbabilityDistribution.bson"))
        println("Running with new EMD isotope model")
    end

    feature_list = DataFrame(CSV.File(path2features))
    comp_order = sortperm(feature_list[:,"Int"], rev = true)
    decon_tab=Matrix(undef,size(feature_list,1),size(feature_list,2)+6);
    # i =2
    for i in ProgressBar(comp_order)
    #@showprogress 1 "Computing..." for i in 1:size(feature_list,1)
    #@time for i=1:size(feature_list,1)
        #println(i)
        # try
            #sleep(0.1)
            #for i in ProgressBar(1:size(feature_list,1))


            if feature_list.ScanNum[i]>0 && feature_list.MeasMass[i]<=max_mass

                feature=deepcopy(feature_list[i,:])

                ind_parent,ind_ms1_comp,mass = ms1_comp_finder(feature,feature_list,chrom,ret_win_per,r_thresh,
                    delta_mass,add_m,ru_m,NL,mode,mass_win_per,Ctype,EMDmodel)

                decon_tab,feature_list = rm_ms1!(ind_parent,ind_ms1_comp,mass,decon_tab,feature_list);

                frag_mz,frag_int,mass_tol=feature_decon_SWATH_singlefeature_internal(chrom,pre_mz,
                    r_thresh,min_int,feature,mass_win_per,ret_win_per,NL)

                if mode == "POSITIVE"
                    mass2rep=feature.MeasMass- 1.007825
                    mass=feature.MeasMass
                elseif mode == "NEGATIVE"
                    mass2rep=feature.MeasMass + 1.007825
                    mass=feature.MeasMass
                end

                if length(frag_mz)>0
                    frag_mz_sel,frag_int_sel,ad_ms2=ms2filter(mass,mass2rep,frag_mz,frag_int,mass_tol,NL,ru_m,delta_mass,add_m,Ctype)
                else
                    frag_mz_sel= []
                    frag_int_sel=[]
                    ad_ms2=[]
                end

                # decon_tab[i,1]

                if length(frag_mz_sel)>0 && ad_ms2 ==1
                    decon_tab[ind_parent[1],15]=1.0
                    decon_tab[ind_parent[1],16]=mass2rep
                    decon_tab[ind_parent[1],19] = string(frag_mz_sel[:])
                    decon_tab[ind_parent[1],20] = string(frag_int_sel[:])
                elseif length(frag_mz_sel)>0 && ad_ms2 ==0
                    decon_tab[ind_parent[1],19] = string(frag_mz_sel[:])
                    decon_tab[ind_parent[1],20] = string(frag_int_sel[:])

                else
                    decon_tab[ind_parent[1],19]=0
                    decon_tab[ind_parent[1],20]=0

                end

            else
                decon_tab[i,:] .= 0
            end

            #println(decon_tab[i,:])

		# catch e
		# 	catch_error(e,i)
		# end

    end
    #println(decon_tab[:,3])

    table=DataFrame(decon_tab,[:Nr,:ScanNum,:ScanInPeak,:Rt,:RtStart,:RtEnd,:MinInPeak,
    :MeasMass,:MinMass,:MaxMass,:Area,:Int,:FeatPurity,:MediRes,:Parent,:AccuMass,:MS1Comp,:MS1CompInt,:MS2Comp,:MS2CompInt])
    sort!(table,[:Nr])
    final_table=table[table[!,:Int] .> 0,:]

    final_table.MS1CompInt[final_table.MS1CompInt.==0] .= "[]"
    final_table.MS1Comp[final_table.MS1Comp.==0] .= "[]"
    final_table.MS2CompInt[final_table.MS2CompInt.==0] .= "[]"
    final_table.MS2Comp[final_table.MS2Comp.==0] .= "[]"
    # final_table.Nr=1:size(final_table,1)
    m=prod(split(path2features,".")[1:end-1] .* ".")[1:end-1]
    output=string(m,"_comp.csv")
    CSV.write(output,final_table)
    println("The final report has been saved in the output path!")
    return final_table

end


#############################################################################
# Wrapping function for componentization of MS1

function comp_ms1(chrom,path2features,mass_win_per,ret_win_per,r_thresh,delta_mass,Ctype,isoModel,fragModel)

    if chrom["MS1"]["Polarity"][1] == "+"
        mode="POSITIVE"
    elseif chrom["MS1"]["Polarity"][1] == "-"
        mode="NEGATIVE"
    end


    max_mass=maximum(chrom["MS1"]["Mz_values"][:])

    pathAUX = "C:\\Users\\dherwer\\OneDrive - UvA\\Algorithm repositories\\Bitbucket\\compcreate.jl\\src"
    path2aux=joinpath(pathAUX,"AUX_data.jld2")
    try
        mm=pathof(CompCreate)
        path2aux=joinpath(mm[1:end-13],"AUX_data.jld2")
    catch
    end

    # println(path2aux)
    a_d=load(path2aux,"AUX_data") # Auxilary data
    # a_d=load("AUX_data.jld2","AUX_data") # Auxilary data

    if mode == "POSITIVE"
        add_m=a_d["Ad"]["pos"]

    elseif mode == "NEGATIVE"

        add_m=a_d["Ad"]["neg"]
    end

    ru_m=a_d["Iso"]
    
    if fragModel == "old"
        NL=a_d["Frag"]
        println("Running with old fragment model")
    else
        NL = a_d["FragCNL"]
        println("Running with new fragment (CNL) model")
    end

    if isoModel == "old"
        EMDmodel = []
        println("Running with old isotope model")
    else
        path = pathof(CompCreate)[1:end-13]
        # path = "C:\\Users\\dherwer\\OneDrive - UvA\\Algorithm repositories\\Bitbucket\\compcreate.jl\\src"
        EMDmodel = BSON.load(joinpath(path,"ProbabilityDistribution.bson"))
        println("Running with new EMD isotope model")
    end

    #feature_list=CSV.read(path2features; copycols=true)
    feature_list = DataFrame(CSV.File(path2features))
    comp_order = sortperm(feature_list[:,"Int"], rev = true)
    decon_tab=Matrix(undef,size(feature_list,1),size(feature_list,2)+4)
    # decon_tab=Matrix(undef,300,size(feature_list,2)+4)

    for i in ProgressBar(comp_order)
        if feature_list.ScanNum[i]>0 && feature_list.MeasMass[i]<=max_mass
            feature=deepcopy(feature_list[i,:])
            ind_parent,ind_ms1_comp,mass = ms1_comp_finder(feature,feature_list,chrom,ret_win_per,r_thresh,
            delta_mass,add_m,ru_m,NL,mode,mass_win_per,Ctype,EMDmodel)
            decon_tab,feature_list = rm_ms1!(ind_parent,ind_ms1_comp,mass,decon_tab,feature_list)
        else
            decon_tab[i,:] .= 0
        end
    end

    table=DataFrame(decon_tab,[:Nr,:ScanNum,:ScanInPeak,:Rt,:RtStart,:RtEnd,:MinInPeak,
    :MeasMass,:MinMass,:MaxMass,:Area,:Int,:FeatPurity,:MediRes,:Parent,:AccuMass,:MS1Comp,:MS1CompInt])
    sort!(table,[:Nr])
    final_table=table[table[!,:Int] .> 0,:]
    
    final_table.MS1CompInt[final_table.MS1CompInt.==0] .= "[]"
    final_table.MS1Comp[final_table.MS1Comp.==0] .= "[]"
    # final_table.Nr=1:size(final_table,1)

    m=prod(split(path2features,".")[1:end-1] .* ".")[1:end-1]
    output=string(m,"_compMS1.csv")

    CSV.write(output,final_table)
    println("The final report has been saved in the output path!")
    return(final_table)

end


#############################################################################
# Wrapping function for componentization of DIA signal


function comp_DIA(chrom,path2features,mass_win_per,ret_win_per,r_thresh,delta_mass,min_int; kwargs...)

    if chrom["MS1"]["Polarity"][1] == "+"
        mode="POSITIVE"
    elseif chrom["MS1"]["Polarity"][1] == "-"
        mode="NEGATIVE"
    end

    GCionisations = ["electron ionization" "electron ionisation" "EI" "APCI"]
    if !(any(keys(kwargs) .== :chromType)) && any(chrom["MS_Instrument"]["msIonisation"] .== GCionisations)
        println("This file will be processed as an GC file. If this is incorrect, please provide the correct chromatography mode by adding chromType = \"GC\" or \"LC\" to the function.\nFor example: compcreate(chrom,path2features,mass_win_per,ret_win_per,r_thresh,delta_mass,min_int, chromType = \"LC\")")
        Ctype = "GC"
    elseif !(any(keys(kwargs) .== :chromType))
        #LC
        println("This file will be processed as an LC file, using the CNL model. If this is incorrect, please provide the correct chromatography mode by adding chromType = \"GC\" or \"LC\" to the function.\nFor example: compcreate(chrom,path2features,mass_win_per,ret_win_per,r_thresh,delta_mass,min_int, chromType = \"GC\")")
        Ctype = "LC"
    else
        #obtain chromatography type from kwargs
        Ctype = kwargs[:chromType]
        println("This file will be processed as specified by the used: "*Ctype)
    end

    pre_mz = unique(chrom["MS2"]["PrecursorIon"])
    prec_win = chrom["MS2"]["PrecursorIonWin"][1:length(pre_mz)]
    tv=chrom["MS1"]["Mz_values"][:];
    ms1_win=maximum(chrom["MS1"]["Mz_values"][:])-minimum(tv[tv .> 0])

    if length(prec_win) > 1 && length(unique(prec_win)) > 3 && minimum(prec_win) > 1


        #SWATH
        println("This file will be processed as a SWATH file.")
        final_table=comp_SWATH_ESI(chrom,path2features,mass_win_per,
            ret_win_per,r_thresh,mode,delta_mass,pre_mz,min_int,Ctype)

        elseif length(prec_win) == 1
            r_m2 = chrom["MS2"]["Rt"][2]
            ind_r_m1 = findfirst(x -> x == r_m2,chrom["MS1"]["Rt"])
            t_rt = chrom["MS1"]["Rt"][ind_r_m1]
            cl_n = length(chrom["MS2"]["Rt"][chrom["MS2"]["Rt"] .== t_rt])
            #println(cl_n)

            if cl_n == 1
                # DIA
                println("This file will be processed as a normal DIA file.")
                final_table = comp_DIA_ESI(chrom,path2features,mass_win_per,
                    ret_win_per,r_thresh,mode,delta_mass,min_int,Ctype)

            elseif cl_n >1
                # DIA multi-collision
                println("This file will be processed as a multicollision one.")
                final_table=comp_DIA_ESI_mc(chrom,path2features,mass_win_per,
                        ret_win_per,r_thresh,mode,delta_mass,min_int,Ctype)
            end

    else

        return(println("The chromatogram provided is not correct! Please try again with a correct file."))
    end



end


#############################################################################
# Wrapping function for componentization all files



function compcreate(chrom,path2features,mass_win_per,ret_win_per,r_thresh,delta_mass,min_int; kwargs...)

    if chrom["MS1"]["Polarity"][1] == "+"
        mode="POSITIVE"
    elseif chrom["MS1"]["Polarity"][1] == "-"
        mode="NEGATIVE"
    end

    if haskey(chrom, "MS2") == 1
        wf = 1
    elseif haskey(chrom, "MS2") == 0
        wf = 2
    else
        println("The data file is not a corrcet one!")
        return wf =3
    end

    #determine ionisation/chromatography type
    GCionisations = ["electron ionization" "electron ionisation" "EI"]
    if !(any(keys(kwargs) .== :chromType)) && any(chrom["MS_Instrument"]["msIonisation"] .== GCionisations)
        println("This file will be processed as an GC file. If this is incorrect, please provide the correct chromatography mode by adding chromType = \"GC\" or \"LC\" to the function.\nFor example: compcreate(chrom,path2features,mass_win_per,ret_win_per,r_thresh,delta_mass,min_int, chromType = \"LC\")")
        Ctype = "GC"
    elseif !(any(keys(kwargs) .== :chromType))
        #LC
        println("This file will be processed as an LC file, using the CNL model. If this is incorrect, please provide the correct chromatography mode by adding chromType = \"GC\" or \"LC\" to the function.\nFor example: compcreate(chrom,path2features,mass_win_per,ret_win_per,r_thresh,delta_mass,min_int, chromType = \"GC\")")
        Ctype = "LC"
    else
        #obtain chromatography type from kwargs
        Ctype = kwargs[:chromType]
        if all(Ctype .!= ["GC" "LC"])
            println("Chromatography mode \"$Ctype\" unknown. Please use either \"GC\" or \"LC\" for the chromType argument")
            return
        end
        println("This file will be processed as specified by the user: "*Ctype)
    end

    if any(keys(kwargs) .== :isoModel)
        isoModel = lowercase(kwargs[:isoModel])
    else
        isoModel = "" #default method
    end

    if any(keys(kwargs) .== :fragModel)
        fragModel = lowercase(kwargs[:fragModel])
    else
        fragModel = "" #default method
    end



    if wf == 1
        pre_mz1 = Float64.(unique(round.(chrom["MS2"]["PrecursorIon"],digits=2)))
        pre_mz = Float64.(unique(round.(chrom["MS2"]["PrecursorIon"],digits=3)))
        prec_win = chrom["MS2"]["PrecursorIonWin"][1:length(pre_mz1)]
        tv=chrom["MS1"]["Mz_values"][:];
        ms1_win=maximum(chrom["MS1"]["Mz_values"][:])-minimum(tv[tv .> 0])

        if length(prec_win) > 1 && length(unique(prec_win)) >= 3 && minimum(prec_win) > 1 && length(chrom["MS2"]["PrecursorIon"])/length(pre_mz1) >= length(pre_mz1)


            #SWATH
            println("This file will be processed as a SWATH file.")
            final_table=comp_SWATH_ESI(chrom,path2features,mass_win_per,
                ret_win_per,r_thresh,mode,delta_mass,pre_mz,min_int,Ctype, isoModel, fragModel)

        elseif length(unique(pre_mz1)) <= 3
            r_m2 = chrom["MS2"]["Rt"][2]
            ind_r_m1 = findfirst(x -> x >= r_m2,chrom["MS1"]["Rt"])
            t_rt = chrom["MS1"]["Rt"][ind_r_m1]
            t_rt2 = chrom["MS1"]["Rt"][ind_r_m1 + 1]
            cl_n = length(findall(x -> t_rt2 >= x >= t_rt,chrom["MS2"]["Rt"]))

            if cl_n == 1
                # DIA
                println("This file will be processed as a normal DIA file.")
                final_table = comp_DIA_ESI(chrom,path2features,mass_win_per,
                    ret_win_per,r_thresh,mode,delta_mass,min_int,Ctype, isoModel, fragModel)

            elseif cl_n >1
                # DIA multi-collision
                println("This file will be processed as a multicollision one.")
                final_table=comp_DIA_ESI_mc(chrom,path2features,mass_win_per,
                        ret_win_per,r_thresh,mode,delta_mass,min_int,Ctype, isoModel, fragModel)
            end

        elseif length(unique(pre_mz1)) > 3 && length(chrom["MS2"]["PrecursorIon"])/length(pre_mz1) < length(pre_mz1)

            # DDA
            println("This file will be processed as a DDA file.")
            final_table = comp_DDA_ESI(chrom,path2features,mass_win_per,
                ret_win_per,r_thresh,mode,delta_mass,min_int,Ctype, isoModel, fragModel)
        else
            println("This file type cannot be selected automatically. You need to process this file using the file specific function.")
        end

    elseif wf == 2
        println("This file will be processed only at MS1 level due to the lack of MS2 level.")
        final_table = comp_ms1(chrom,path2features,mass_win_per,ret_win_per,r_thresh,delta_mass,Ctype, isoModel, fragModel)

    else
        throw(MethodError(-1, "CompCreate could not automatically determine MS measurement type"))
    end



end



##############################################################################
# # Testing area
# # """
# using MS_Import
# using Plots
# using SAFD

# include("C:\\Users\\dherwer\\OneDrive - UvA\\Algorithm repositories\\Bitbucket\\compcreate.jl\\src\\KMD.jl")
# include("C:\\Users\\dherwer\\OneDrive - UvA\\Algorithm repositories\\Bitbucket\\compcreate.jl\\src\\Peak_finder.jl")

# # pathin="/Volumes/SAER HD/Data/Temp_files/Inspectra_test"
# #pathin="/Users/saersamanipour/Desktop/UvA/tempfiles"
# # filenames=["ddMS2L_IE1_LPool_Neg.mzXML"]
# # mz_thresh=[0,550]
# # int_thresh=500
# # ch=import_files(pathin,filenames,mz_thresh,int_thresh)
# # chrom = deepcopy(ch)
# #
# # !!!
# pathin = "C:\\Users\\dherwer\\OneDrive - UvA\\HRMStoolbox\\ICNTS data\\"
# filenames = ["50-50_EtOH-Water_1ul_AllIon_pos_1.mzXML"]
# chrom=import_files(pathin,filenames,[0,0])

# # max_numb_iter=100
# # max_t_peak_w=300
# # res=20000
# # min_ms_w=0.02
# # r_thresh=0.75
# # min_int=500
# # sig_inc_thresh=5
# # S2N=2
# # min_peak_w_s=3


# mass_win_per=0.75
# ret_win_per=0.5
# delta_mass=0.004
# min_int=100


# path2features=joinpath(pathin,filenames[1][1:end-6] * "_report.csv")

# # # # mode = "POSITIVE" #"NEGATIVE"
# # #  #path2features=joinpath(pathin,"B_1_SAFD_399.csv")
# # #
# # # @time safd_s3D(chrom["MS1"]["Mz_values"],chrom["MS1"]["Mz_intensity" ],chrom["MS1"]["Rt" ],
# # #    filenames[1][1:end-6],pathin,max_numb_iter,max_t_peak_w,res,min_ms_w,r_thresh,min_int,sig_inc_thresh,S2N,min_peak_w_s)
# # #
# @time comp=compcreate(chrom,path2features,mass_win_per,ret_win_per,r_thresh,delta_mass,min_int)
# #
# # """




# ## test Gijs data

#using MS_Import
#pathin = "C:\\Users\\dherwer\\Desktop\\"
#filenames = ["240126_3M1000_MMW_MS1_an1.mzXML"]

#chrom = import_files(pathin,filenames,[0,0])

#mass_win_per=0.8
#ret_win_per=0.5
#r_thresh=0.8
#delta_mass=0.004
#min_int=2000
#MS1
#path2features=joinpath(pathin,"240126_3M1000_MMW_MS1_BS_report.csv")

#@time comp=compcreate(chrom,path2features,mass_win_per,ret_win_per,r_thresh,delta_mass,min_int)

#testing other file types
#pathin = "C:\\Users\\dherwer\\OneDrive - UvA\\CNL\\Data\\UR"
#filenames = ["07_PestMix-6_1mg-L_0-5ul_pos.mzXML"]
#chrom = import_files(pathin,filenames,[0,0])
#path2features=joinpath(pathin,filenames[1][1:end-6] * "_report.csv")
#mode = "POSITIVE"
#@time comp=compcreate(chrom,path2features,mass_win_per,ret_win_per,r_thresh,delta_mass,min_int)

#SWATH






# # ## new CNL model
# pathAUX = "C:\\Users\\dherwer\\OneDrive - UvA\\Algorithm repositories\\Bitbucket\\compcreate.jl\\src"
# path2aux=joinpath(pathAUX,"AUX_data.jld2")
# AUX_data=load(path2aux,"AUX_data") 

# using JLD
# cnl=load("C:/Users/dherwer/Desktop/Denice_van_Herwerden-cnlforfragments.jl-2fc342d7ed28/models/CNLmodel_2023-06-05.jld")
# #load CNL
# ind = findall(cnl["def_score"] .>= 0.8)#cnl["def_scoreThresh"])
# mass = unique(roundby.(cnl["mass"][ind], 0.01))
# AUX_data["FragCNL"] = Dict("mass" => mass, 
#             "prob" => ones(length(mass)))

# save(path2aux,Dict("AUX_data" => AUX_data))       
