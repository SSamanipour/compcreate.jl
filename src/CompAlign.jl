using CSV
using DataFrames
using Glob
using Statistics
using XLSX


###########################################
# A function to import the files

# path2reports = path2files
function report_import_internal(path2reports)

    #MS2 included files
    ms2files = false
    nn = readdir(path2reports)
    Rep = DataFrame()
    namest = []
    c = 1
    for i =1:size(nn,1)
        if isdir(joinpath(path2reports,nn[i]))
            continue
        end
        println(i)
        m = split(nn[i,:][1],".")
        if length(m[1]) == 0 || m[end] != "csv" || !contains(m[end-1],"report_comp") || (m[end-1][end-10:end] != "report_comp")
            continue

        end 
        f_n = joinpath(path2reports,nn[i,:][1])
        df = CSV.read(f_n, DataFrame)
        df1 = hcat(DataFrame(F_ID= c .* Int.(ones(size(df,1)))),df)
        Rep = vcat(Rep,df1)
        namest = vcat(namest,nn[i])
        c = c+1
        ms2files = true
    end 

    #same for MS1 only files
    for i =1:size(nn,1)
        if isdir(joinpath(path2reports,nn[i]))
            continue
        end
        m = split(nn[i,:][1],".")
        if length(m[1]) == 0 || m[end] != "csv" || !contains(m[end-1],"report_compMS1") || (m[end-1][end-13:end] != "report_compMS1")
            continue

        end 
        f_n = joinpath(path2reports,nn[i,:][1])
        df = CSV.read(f_n, DataFrame)
        df1 = hcat(DataFrame(F_ID= c .* Int.(ones(size(df,1)))),df)
        #add columns
        df1 = hcat(df1, DataFrame(MS2Comp = fill("MS1only",size(df1,1)), MS2CompInt = "MS1only"))
        Rep = vcat(Rep,df1)
        namest = vcat(namest,nn[i])
        c = c+1

    end 
    # for j = 1:size(Rep, 2)-4
    #     if (typeof(Rep[:,j]) .== Vector{Any}) ||  (typeof(Rep[:,j]) .== Vector{Float64})
    #         Rep[:,j] = Float64.(Rep[:,j])
    #     end
    # end


    # Rep = Rep[vec(.! any(Matrix(ismissing.(Rep)), dims = 2)),:]

    return(Rep,namest)
end


###########################################
# A function to select candidate featuters retention time 

function select_candidates_rt(rep,ind)

    pf = rep[ind,:]
    med_scan = median(rep.ScanInPeak)

    if pf.ScanNum - med_scan <= 0
        lb = 1
        ub = pf.ScanNum + med_scan
    elseif pf.ScanNum + med_scan >= maximum(rep.ScanNum)
        lb = pf.ScanNum - med_scan
        ub =  maximum(rep.ScanNum)
    elseif pf.ScanNum - med_scan > 0 && pf.ScanNum + med_scan < maximum(rep.ScanNum)
        lb = pf.ScanNum - med_scan
        ub = pf.ScanNum + med_scan
    end 
    
    sel_inds = findall(x -> ub >= x >= lb, rep.ScanNum)

    return sel_inds
end


###########################################
# A function to select candidate featuters retention time 

function select_candidates_rt_(rep,ind,rt_tol)

    pf = rep[ind,:]
    

    if pf.Rt - rt_tol <= 0
        lb = 0
        ub = pf.Rt + rt_tol
    elseif pf.Rt + rt_tol >= maximum(rep.Rt)
        lb = pf.Rt - rt_tol
        ub =  maximum(rep.Rt)
    elseif pf.Rt - rt_tol > 0 && pf.Rt + rt_tol < maximum(rep.Rt)
        lb = pf.Rt - rt_tol
        ub = pf.Rt + rt_tol
    end 
    
    sel_inds = findall(x -> ub >= x >= lb, rep.Rt)

    return sel_inds
end


#####################################################################################
# A function to select candidate featuters mass 

function select_candidates_mz(rep,ind,sel_inds)

    pf = rep[ind,:]
    
    s_f = rep[sel_inds,:]

    med_mz_tol = median(s_f.MaxMass .- s_f.MinMass)

    
    sel_inds_f = sel_inds[findall(x -> pf.MeasMass + med_mz_tol >= x >= pf.MeasMass - med_mz_tol, rep.MeasMass[sel_inds])]

    return sel_inds_f
end

#####################################################################################
# A function to select candidate featuters mass with a mass tolerances

function select_candidates_mz_(rep,ind,sel_inds,mz_tol)

    pf = rep[ind,:]

    
    sel_inds_f = sel_inds[findall(x -> pf.MeasMass + mz_tol >= x >= pf.MeasMass - mz_tol, rep.MeasMass[sel_inds])]

    return sel_inds_f
end

#####################################################################################
# A function to group select candidate featuters 

function group_candidates_internal(rep,sel_inds_f)

    #pf = rep[ind,:]
    #sel_inds_f1 = deepcopy(sel_inds_f)
    

    AvScan = round(mean(rep.ScanNum[sel_inds_f]))
    MaxScanInPeak = maximum(rep.ScanInPeak[sel_inds_f])
    MinRt = minimum(rep.RtStart[sel_inds_f])
    MaxRt = maximum(rep.RtEnd[sel_inds_f])
    AveRt = round(mean([MinRt,MaxRt]),digits =2)
    MinMass = minimum(rep.MinMass[sel_inds_f])
    MaxMass = maximum(rep.MaxMass[sel_inds_f])
    AveMass = round(mean(rep.MeasMass[sel_inds_f]),digits =4)

    


    # ind_files = findall(x -> x ==1, rep.Nr)
    ind_files = []
    for i in unique(rep.F_ID)
        ind_files = append!(ind_files, findfirst(rep.F_ID .== i))
    end

    Int_ = zeros(1,length(ind_files))
    Area = zeros(1,length(ind_files))
    Res = zeros(1,length(ind_files))
    Num = fill("NA",size(Res))
    MS1mz = fill("0",size(Res))
    MS1int = fill("0",size(Res))
    MS2mz = fill("0",size(Res))
    MS2int = fill("0",size(Res))
    
   for i =1:length(ind_files)
        #println(i)

        sele_f = rep[sel_inds_f,:]

        selected_f = sele_f[sele_f.F_ID .== i,:]
        if size(selected_f,1) >1
            Int_[i] = maximum(selected_f.Int)
            Area[i] = maximum(selected_f.Area)
            ord = sortperm(selected_f.Int, rev = true)
            Num[i] = string(selected_f.Nr[ord])
            MS1mz[i] = string(selected_f.MS1Comp[ord])
            MS1int[i] = string(selected_f.MS1CompInt[ord])
            MS2mz[i] = string(selected_f.MS2Comp[ord])
            MS2int[i] = string(selected_f.MS2CompInt[ord])
            temp_r = selected_f.MediRes[selected_f.MediRes .!= Inf]
            if isempty(temp_r)
                Res[i] = Inf
            else
                Res[i] = median(temp_r)
            end
        elseif size(selected_f,1) == 1
            Int_[i] = selected_f.Int[1]
            Area[i] = selected_f.Area[1]
            Res[i] = selected_f.MediRes[1]
            Num[i] = string(selected_f.Nr[1])
            MS1mz[i] = selected_f.MS1Comp[1]
            MS1int[i] = selected_f.MS1CompInt[1]
            MS2mz[i] = selected_f.MS2Comp[1]
            MS2int[i] = selected_f.MS2CompInt[1]
            

        end 
        

   end 

    return(AvScan,MaxScanInPeak, MinRt,MaxRt,AveRt,MinMass,MaxMass,AveMass,Int_,Area,Res,Num,MS1mz,MS1int,MS2mz,MS2int)
end

function group_candidates_internalN(reps,ind_files)

    #pf = reps[ind,:]
    #sel_inds_f1 = deepcopy(sel_inds_f)
    

    AvScan = round(mean(reps.ScanNum))
    MaxScanInPeak = maximum(reps.ScanInPeak)
    MinRt = minimum(reps.RtStart)
    MaxRt = maximum(reps.RtEnd)
    AveRt = round(mean([MinRt,MaxRt]),digits =2)
    MinMass = minimum(reps.MinMass)
    MaxMass = maximum(reps.MaxMass)
    AveMass = round(mean(reps.MeasMass),digits =4)

    


    # ind_files = findall(x -> x ==1, reps.Nr)
    # ind_files = []
    # for i in unique(reps.F_ID)
        # ind_files = append!(ind_files, findfirst(reps.F_ID .== i))
    # end

    Int_ = zeros(1,length(ind_files))
    Area = zeros(1,length(ind_files))
    Res = zeros(1,length(ind_files))
    Num = fill("NA",size(Res))
    MS1mz = fill("0",size(Res))
    MS1int = fill("0",size(Res))
    MS2mz = fill("0",size(Res))
    MS2int = fill("0",size(Res))
    
   for i =1:length(ind_files)
        #println(i)

        # sele_f = rep[sel_inds_f,:]

        # selected_f = sele_f[sele_f.F_ID .== i,:]
        selected_f = reps[reps.F_ID .== i,:]
        if size(selected_f,1) >1
            Int_[i] = maximum(selected_f.Int)
            Area[i] = maximum(selected_f.Area)
            ord = sortperm(selected_f.Int, rev = true)
            Num[i] = string(selected_f.Nr[ord])
            MS1mz[i] = string(selected_f.MS1Comp[ord])
            MS1int[i] = string(selected_f.MS1CompInt[ord])
            MS2mz[i] = string(selected_f.MS2Comp[ord])
            MS2int[i] = string(selected_f.MS2CompInt[ord])
            temp_r = selected_f.MediRes[selected_f.MediRes .!= Inf]
            if isempty(temp_r)
                Res[i] = Inf
            else
                Res[i] = median(temp_r)
            end
        elseif size(selected_f,1) == 1
            Int_[i] = selected_f.Int[1]
            Area[i] = selected_f.Area[1]
            Res[i] = selected_f.MediRes[1]
            Num[i] = string(selected_f.Nr[1])
            MS1mz[i] = selected_f.MS1Comp[1]
            MS1int[i] = selected_f.MS1CompInt[1]
            MS2mz[i] = selected_f.MS2Comp[1]
            MS2int[i] = selected_f.MS2CompInt[1]
            

        end 
        

   end 

    return(AvScan,MaxScanInPeak, MinRt,MaxRt,AveRt,MinMass,MaxMass,AveMass,Int_,Area,Res,Num,MS1mz,MS1int,MS2mz,MS2int)
end

#####################################################################################
# A function to align features generated via SAFD 

function component_align_internal(path2files)

    rep,name = report_import_internal(path2files)

    if length(name) ==0
        @warn "No file has been imported."
        return res =[]
    end 

    MS1only = all(rep[!,"MS2Comp"] .== "MS1only")

    feature = zeros(1,8)
    Inten = zeros(1,length(name))
    Area = zeros(1,length(name))
    Res = zeros(1,length(name))
    Num = fill("NA", size(Res))
    MS1mz = fill("0", size(Res))
    MS1int = fill("0", size(Res))
    if !MS1only
        MS2mz = fill("0", size(Res))
        MS2int = fill("0", size(Res))
    end
    
    for i =1:size(rep,1)
        if rep.Rt[i] == 0
            continue
        end 
        ind = i
        println(i)
        sel_inds = select_candidates_rt(rep,ind)
        sel_inds_f = select_candidates_mz(rep,ind,sel_inds)
        AvScan,MaxScanInPeak, MinRt,MaxRt,AveRt,MinMass,MaxMass,AveMass,Int_,Area_,Res_,Num_,MS1mz_,MS1int_,MS2mz_,MS2int_ = group_candidates_internal(rep,sel_inds_f)
        rep.Rt[sel_inds_f] .= 0
        feature = vcat(feature,hcat(AvScan,MaxScanInPeak, MinRt,MaxRt,AveRt,MinMass,MaxMass,AveMass))
        Inten = vcat(Inten,Int_)
        Area = vcat(Area,Area_)
        Res = vcat(Res,Res_)
        Num = vcat(Num,Num_)
        MS1mz = vcat(MS1mz,MS1mz_)
        MS1int = vcat(MS1int,MS1int_)
        if !MS1only
            MS2mz = vcat(MS2mz,MS2mz_)
            MS2int = vcat(MS2int,MS2int_)
        end

    end 

    table1=DataFrame(hcat(1:size( feature,1)-1,feature[2:end,:]),[:Nr,:AveScanNum,:MaxScanInPeak,:MinRt,:MaxRt,:AveRt,
    :MinMass,:MaxMass,:AveMass])
    

    table_int=DataFrame(Inten[2:end,:], :auto)
    table_are=DataFrame(Area[2:end,:], :auto)
    table_res=DataFrame(Res[2:end,:], :auto)
    table_num=DataFrame(Num[2:end,:], :auto)
    table_MS1mz=DataFrame(MS1mz[2:end,:], :auto)
    table_MS1int=DataFrame(MS1int[2:end,:], :auto)
    if !MS1only
        table_MS2mz=DataFrame(MS2mz[2:end,:], :auto)
        table_MS2int=DataFrame(MS2int[2:end,:], :auto)
    end

    rename!(table_int,Symbol.(name))
    rename!(table_are,Symbol.(name))
    rename!(table_res,Symbol.(name))
    rename!(table_num,Symbol.(name))
    rename!(table_MS1mz,Symbol.(name))
    rename!(table_MS1int,Symbol.(name))
    if !MS1only
        rename!(table_MS2mz,Symbol.(name))
        rename!(table_MS2int,Symbol.(name))
    end

    table_int_f=hcat(table1,table_int)
    table_are_f=hcat(table1,table_are)
    table_res_f=hcat(table1,table_res)
    table_num_f=hcat(table1,table_num)
    table_MS1mz_f=hcat(table1,table_MS1mz)
    table_MS1int_f=hcat(table1,table_MS1int)
    if !MS1only
        table_MS2mz_f=hcat(table1,table_MS2mz)
        table_MS2int_f=hcat(table1,table_MS2int)
    end

    sort!(table_int_f,[:AveScanNum,:AveMass])
    sort!(table_are_f,[:AveScanNum,:AveMass])
    sort!(table_res_f,[:AveScanNum,:AveMass])
    sort!(table_num_f,[:AveScanNum,:AveMass])
    sort!(table_MS1mz_f,[:AveScanNum,:AveMass])
    sort!(table_MS1int_f,[:AveScanNum,:AveMass])
    if !MS1only
        sort!(table_MS2mz_f,[:AveScanNum,:AveMass])
        sort!(table_MS2int_f,[:AveScanNum,:AveMass])
    end

    table_int_f.Nr = 1:size( feature,1)-1
    table_are_f.Nr = 1:size( feature,1)-1
    table_res_f.Nr = 1:size( feature,1)-1
    table_num_f.Nr = 1:size( feature,1)-1
    table_MS1mz_f.Nr = 1:size( feature,1)-1
    table_MS1int_f.Nr = 1:size( feature,1)-1
    if !MS1only
        table_MS2mz_f.Nr = 1:size( feature,1)-1
        table_MS2int_f.Nr = 1:size( feature,1)-1
    end

    output_int=joinpath(path2files,"ComponentList_Aligned_int.csv")
    output_area=joinpath(path2files,"ComponentList_Aligned_area.csv")
    output_res=joinpath(path2files,"ComponentList_Aligned_res.csv")
    output_num=joinpath(path2files,"ComponentList_Aligned_compNum.csv")
    output_MS1mz=joinpath(path2files,"ComponentList_Aligned_MS1mz.csv")
    output_MS1int=joinpath(path2files,"ComponentList_Aligned_MS1int.csv")
    if !MS1only
        output_MS2mz=joinpath(path2files,"ComponentList_Aligned_MS2mz.csv")
        output_MS2int=joinpath(path2files,"ComponentList_Aligned_MS2int.csv")
    end

    CSV.write(output_int,table_int_f)
    CSV.write(output_area,table_are_f)
    CSV.write(output_res,table_res_f)
    CSV.write(output_num,table_num_f)
    CSV.write(output_MS1mz,table_MS1mz_f)
    CSV.write(output_MS1int,table_MS1int_f)
    if !MS1only
        CSV.write(output_MS2mz,table_MS2mz_f)
        CSV.write(output_MS2int,table_MS2int_f)
    end
    #XLSX.writetable(output, Intensities=(collect(DataFrames.eachcol(table_int_f)),
    # DataFrames.names(table_int_f)  ), Areas=(collect(DataFrames.eachcol(table_are_f)),
    #  DataFrames.names(table_are_f)  ), Resolutions=(collect(DataFrames.eachcol(table_res_f)),
    #   DataFrames.names(table_res_f)  ), overwrite=true)

    if MS1only
        return(table_int_f,table_are_f,table_res_f,table_num_f,table_MS1mz_f,table_MS1int_f)
    else
        return(table_int_f,table_are_f,table_res_f,table_num_f,table_MS1mz_f,table_MS1int_f,table_MS2mz_f,table_MS2int_f)
    end
end

#####################################################################################
# A function to align features generated via CompCreate that aligns smaller parts
function feature_align_fraction(rep, name, ind_files, feature, Inten, Area, Res, Num, MS1mz, MS1int, MS2mz, MS2int, MS1only, maxRTmatch, mz_tol, rt_tol)
    
    
    
    outside = sum(rep.Rt .> maxRTmatch)
    i = 1
    while i <= (size(rep,1) - outside)
        if rep.Rt[i] == 0 || rep.Rt[i] > maxRTmatch
            i += 1
            continue
        end 

        ind = i
      
        if rt_tol == 0
            sel_inds = select_candidates_rt(rep,ind)
        else
            sel_inds = select_candidates_rt_(rep,ind,rt_tol)
        end
        if mz_tol == 0
            sel_inds_f = select_candidates_mz(rep,ind,sel_inds)
        else
            sel_inds_f = select_candidates_mz_(rep,ind,sel_inds, mz_tol)
        end
        # sel_inds = select_candidates_rt(rep,ind)
        # sel_inds_f = select_candidates_mz(rep,ind,sel_inds)
        AvScan,MaxScanInPeak, MinRt,MaxRt,AveRt,MinMass,MaxMass,AveMass,Int_,Area_,Res_,Num_,MS1mz_,MS1int_,MS2mz_,MS2int_ = group_candidates_internalN(rep[sel_inds_f,:],ind_files)
        rep.Rt[sel_inds_f] .= 0
        feature = vcat(feature,hcat(AvScan,MaxScanInPeak, MinRt,MaxRt,AveRt,MinMass,MaxMass,AveMass))
        Inten = vcat(Inten,Int_)
        Area = vcat(Area,Area_)
        Res = vcat(Res,Res_)
        Num = vcat(Num,Num_)
        MS1mz = vcat(MS1mz,MS1mz_)
        MS1int = vcat(MS1int,MS1int_)
        if !MS1only
            MS2mz = vcat(MS2mz,MS2mz_)
            MS2int = vcat(MS2int,MS2int_)
        end


        if mod(i,50) == 0
            #reduce rep
            rep = rep[rep.Rt .!= 0,:]
            println("Part reduced to $(size(rep,1))")
            #assuming that everyting before always gets matched
            i = 0
        end
        i += 1
    end 
    #get ungrouped features outside of time domain
    rep_rem = rep
    return feature, Inten, Area, Res, Num, MS1mz, MS1int, MS2mz, MS2int, rep_rem
end

function component_align_internal_parts(path2files, tr1_parts = 5, rt_tol_p = 0.1, mz_tol = 0., rt_tol = 0.)

    rep,name = report_import_internal(path2files)

    if length(name) ==0
        @warn "No file has been imported."
        return res =[]
    end 

    MS1only = all(rep[!,"MS2Comp"] .== "MS1only")

    feature = zeros(1,8)
    Inten = zeros(1,length(name))
    Area = zeros(1,length(name))
    Res = zeros(1,length(name))
    Num = fill("NA", size(Res))
    MS1mz = fill("0", size(Res))
    MS1int = fill("0", size(Res))
    MS2mz = fill("0", size(Res))
    MS2int = fill("0", size(Res))


    ind_files = unique(rep.F_ID)

    tr1_part_lim = zeros(tr1_parts+1)
    tr1_part_lim[1] = minimum(rep[!,"Rt"])
    tr1_part_lim[end] = maximum(rep[!,"Rt"])
    rtps = sort(rep[!,"Rt"])
    for p = 2:tr1_parts
        tr1_part_lim[p] = rtps[Int(round((length(rtps)/tr1_parts)*(p-1)))] # select retention time at specific part of data
    end


    #run alignment on part of the code
    for t = 1:length(tr1_part_lim)-1
        ind_part = findall((tr1_part_lim[1] - 3*rt_tol_p) .<= rep[!,"Rt"] .<= (tr1_part_lim[t+1] + 1*rt_tol_p))
        println("Part $(t)/$tr1_parts: passing $(length(ind_part)) features")
        maxRTmatch = tr1_part_lim[t+1]
        feature, Inten, Area, Res, Num, MS1mz, MS1int, MS2mz, MS2int, rep_rem = feature_align_fraction(rep[ind_part,:], name, ind_files, feature, Inten, Area, Res, Num, MS1mz, MS1int, MS2mz, MS2int, MS1only, maxRTmatch,mz_tol,rt_tol)
        rep = [rep[.!((tr1_part_lim[1] - 3*rt_tol_p) .<= rep[!,"Rt"] .<= (tr1_part_lim[t+1] + 1*rt_tol_p)),:] ; rep_rem]
        println("Features remaining in rep $(size(rep,1))")
    end


    # for i =1:size(rep,1)
    i = 1
    println("Final part: passing $(size(rep,1)) features")
    while i <= size(rep,1) && !(all(rep.Rt .== 0))
        if rep.Rt[i] == 0
            continue
        end 
        ind = i
        println(i)
        if rt_tol == 0
            sel_inds = select_candidates_rt(rep,ind)
        else
            sel_inds = select_candidates_rt_(rep,ind,rt_tol)
        end
        if mz_tol == 0
            sel_inds_f = select_candidates_mz(rep,ind,sel_inds)
        else
            sel_inds_f = select_candidates_mz_(rep,ind,sel_inds, mz_tol)
        end
        # sel_inds = select_candidates_rt(rep,ind)
        # sel_inds_f = select_candidates_mz(rep,ind,sel_inds)
        AvScan,MaxScanInPeak, MinRt,MaxRt,AveRt,MinMass,MaxMass,AveMass,Int_,Area_,Res_,Num_,MS1mz_,MS1int_,MS2mz_,MS2int_ = group_candidates_internal(rep,sel_inds_f)
        rep.Rt[sel_inds_f] .= 0
        feature = vcat(feature,hcat(AvScan,MaxScanInPeak, MinRt,MaxRt,AveRt,MinMass,MaxMass,AveMass))
        Inten = vcat(Inten,Int_)
        Area = vcat(Area,Area_)
        Res = vcat(Res,Res_)
        Num = vcat(Num,Num_)
        MS1mz = vcat(MS1mz,MS1mz_)
        MS1int = vcat(MS1int,MS1int_)
        if !MS1only
            MS2mz = vcat(MS2mz,MS2mz_)
            MS2int = vcat(MS2int,MS2int_)
        end

        if mod(i,10) == 0
            #reduce rep
            rep = rep[rep.Rt .!= 0,:]
            println("reduced to $(size(rep,1))")
            #assuming that everyting before always gets matched
            i = 0
        end
        i += 1
    end 

    table1=DataFrame(hcat(1:size( feature,1)-1,feature[2:end,:]),[:Nr,:AveScanNum,:MaxScanInPeak,:MinRt,:MaxRt,:AveRt,
    :MinMass,:MaxMass,:AveMass])
    

    table_int=DataFrame(Inten[2:end,:], :auto)
    table_are=DataFrame(Area[2:end,:], :auto)
    table_res=DataFrame(Res[2:end,:], :auto)
    table_num=DataFrame(Num[2:end,:], :auto)
    table_MS1mz=DataFrame(MS1mz[2:end,:], :auto)
    table_MS1int=DataFrame(MS1int[2:end,:], :auto)
    if !MS1only
        table_MS2mz=DataFrame(MS2mz[2:end,:], :auto)
        table_MS2int=DataFrame(MS2int[2:end,:], :auto)
    end

    rename!(table_int,Symbol.(name))
    rename!(table_are,Symbol.(name))
    rename!(table_res,Symbol.(name))
    rename!(table_num,Symbol.(name))
    rename!(table_MS1mz,Symbol.(name))
    rename!(table_MS1int,Symbol.(name))
    if !MS1only
        rename!(table_MS2mz,Symbol.(name))
        rename!(table_MS2int,Symbol.(name))
    end

    table_int_f=hcat(table1,table_int)
    table_are_f=hcat(table1,table_are)
    table_res_f=hcat(table1,table_res)
    table_num_f=hcat(table1,table_num)
    table_MS1mz_f=hcat(table1,table_MS1mz)
    table_MS1int_f=hcat(table1,table_MS1int)
    if !MS1only
        table_MS2mz_f=hcat(table1,table_MS2mz)
        table_MS2int_f=hcat(table1,table_MS2int)
    end

    sort!(table_int_f,[:AveScanNum,:AveMass])
    sort!(table_are_f,[:AveScanNum,:AveMass])
    sort!(table_res_f,[:AveScanNum,:AveMass])
    sort!(table_num_f,[:AveScanNum,:AveMass])
    sort!(table_MS1mz_f,[:AveScanNum,:AveMass])
    sort!(table_MS1int_f,[:AveScanNum,:AveMass])
    if !MS1only
        sort!(table_MS2mz_f,[:AveScanNum,:AveMass])
        sort!(table_MS2int_f,[:AveScanNum,:AveMass])
    end

    table_int_f.Nr = 1:size( feature,1)-1
    table_are_f.Nr = 1:size( feature,1)-1
    table_res_f.Nr = 1:size( feature,1)-1
    table_num_f.Nr = 1:size( feature,1)-1
    table_MS1mz_f.Nr = 1:size( feature,1)-1
    table_MS1int_f.Nr = 1:size( feature,1)-1
    if !MS1only
        table_MS2mz_f.Nr = 1:size( feature,1)-1
        table_MS2int_f.Nr = 1:size( feature,1)-1
    end

    output_int=joinpath(path2files,"ComponentList_Aligned_int.csv")
    output_area=joinpath(path2files,"ComponentList_Aligned_area.csv")
    output_res=joinpath(path2files,"ComponentList_Aligned_res.csv")
    output_num=joinpath(path2files,"ComponentList_Aligned_compNum.csv")
    output_MS1mz=joinpath(path2files,"ComponentList_Aligned_MS1mz.csv")
    output_MS1int=joinpath(path2files,"ComponentList_Aligned_MS1int.csv")
    if !MS1only
        output_MS2mz=joinpath(path2files,"ComponentList_Aligned_MS2mz.csv")
        output_MS2int=joinpath(path2files,"ComponentList_Aligned_MS2int.csv")
    end

    CSV.write(output_int,table_int_f)
    CSV.write(output_area,table_are_f)
    CSV.write(output_res,table_res_f)
    CSV.write(output_num,table_num_f)
    CSV.write(output_MS1mz,table_MS1mz_f)
    CSV.write(output_MS1int,table_MS1int_f)
    if !MS1only
        CSV.write(output_MS2mz,table_MS2mz_f)
        CSV.write(output_MS2int,table_MS2int_f)
    end
    #XLSX.writetable(output, Intensities=(collect(DataFrames.eachcol(table_int_f)),
    # DataFrames.names(table_int_f)  ), Areas=(collect(DataFrames.eachcol(table_are_f)),
    #  DataFrames.names(table_are_f)  ), Resolutions=(collect(DataFrames.eachcol(table_res_f)),
    #   DataFrames.names(table_res_f)  ), overwrite=true)

    if MS1only
        return(table_int_f,table_are_f,table_res_f,table_num_f,table_MS1mz_f,table_MS1int_f)
    else
        return(table_int_f,table_are_f,table_res_f,table_num_f,table_MS1mz_f,table_MS1int_f,table_MS2mz_f,table_MS2int_f)
    end
end






#####################################################################################
# A function to align features generated via SAFD 

function component_align_internal_wp(path2files,mz_tol,rt_tol)

    rep,name = report_import_internal(path2files)

    if length(name) ==0
        @warn "No file has been imported."
        return res =[]
    end 

    MS1only = all(rep[!,"MS2Comp"] .== "MS1only")

    feature = zeros(1,8)
    Inten = zeros(1,length(name))
    Area = zeros(1,length(name))
    Res = zeros(1,length(name))
    Num = fill("NA", size(Res))
    MS1mz = fill("0", size(Res))
    MS1int = fill("0", size(Res))
    if !MS1only
        MS2mz = fill("0", size(Res))
        MS2int = fill("0", size(Res))
    end
    
    for i =1:size(rep,1)
        if rep.Rt[i] == 0
            continue
        end 
        ind = i
        println(i)
        sel_inds = select_candidates_rt_(rep,ind,rt_tol)
        sel_inds_f = select_candidates_mz_(rep,ind,sel_inds,mz_tol)
        AvScan,MaxScanInPeak, MinRt,MaxRt,AveRt,MinMass,MaxMass,AveMass,Int_,Area_,Res_,Num_,MS1mz_,MS1int_,MS2mz_,MS2int_ = group_candidates_internal(rep,sel_inds_f)
        rep.Rt[sel_inds_f] .= 0
        feature = vcat(feature,hcat(AvScan,MaxScanInPeak, MinRt,MaxRt,AveRt,MinMass,MaxMass,AveMass))
        Inten = vcat(Inten,Int_)
        Area = vcat(Area,Area_)
        Res = vcat(Res,Res_)
        Num = vcat(Num,Num_)
        MS1mz = vcat(MS1mz,MS1mz_)
        MS1int = vcat(MS1int,MS1int_)
        if !MS1only
            MS2mz = vcat(MS2mz,MS2mz_)
            MS2int = vcat(MS2int,MS2int_)
        end
    end 

    table1=DataFrame(hcat(1:size( feature,1)-1,feature[2:end,:]),[:Nr,:AveScanNum,:MaxScanInPeak,:MinRt,:MaxRt,:AveRt,
    :MinMass,:MaxMass,:AveMass])
    

    
    table_int=DataFrame(Inten[2:end,:], :auto)
    table_are=DataFrame(Area[2:end,:], :auto)
    table_res=DataFrame(Res[2:end,:], :auto)
    table_num=DataFrame(Num[2:end,:], :auto)
    table_MS1mz=DataFrame(MS1mz[2:end,:], :auto)
    table_MS1int=DataFrame(MS1int[2:end,:], :auto)
    if !MS1only
        table_MS2mz=DataFrame(MS2mz[2:end,:], :auto)
        table_MS2int=DataFrame(MS2int[2:end,:], :auto)
    end

    rename!(table_int,Symbol.(name))
    rename!(table_are,Symbol.(name))
    rename!(table_res,Symbol.(name))
    rename!(table_num,Symbol.(name))
    rename!(table_MS1mz,Symbol.(name))
    rename!(table_MS1int,Symbol.(name))
    if !MS1only
        rename!(table_MS2mz,Symbol.(name))
        rename!(table_MS2int,Symbol.(name))
    end

    table_int_f=hcat(table1,table_int)
    table_are_f=hcat(table1,table_are)
    table_res_f=hcat(table1,table_res)
    table_num_f=hcat(table1,table_num)
    table_MS1mz_f=hcat(table1,table_MS1mz)
    table_MS1int_f=hcat(table1,table_MS1int)
    if !MS1only
        table_MS2mz_f=hcat(table1,table_MS2mz)
        table_MS2int_f=hcat(table1,table_MS2int)
    end

    sort!(table_int_f,[:AveScanNum,:AveMass])
    sort!(table_are_f,[:AveScanNum,:AveMass])
    sort!(table_res_f,[:AveScanNum,:AveMass])
    sort!(table_num_f,[:AveScanNum,:AveMass])
    sort!(table_MS1mz_f,[:AveScanNum,:AveMass])
    sort!(table_MS1int_f,[:AveScanNum,:AveMass])
    if !MS1only
        sort!(table_MS2mz_f,[:AveScanNum,:AveMass])
        sort!(table_MS2int_f,[:AveScanNum,:AveMass])
    end

    table_int_f.Nr = 1:size( feature,1)-1
    table_are_f.Nr = 1:size( feature,1)-1
    table_res_f.Nr = 1:size( feature,1)-1
    table_num_f.Nr = 1:size( feature,1)-1
    table_MS1mz_f.Nr = 1:size( feature,1)-1
    table_MS1int_f.Nr = 1:size( feature,1)-1
    if !MS1only
        table_MS2mz_f.Nr = 1:size( feature,1)-1
        table_MS2int_f.Nr = 1:size( feature,1)-1
    end

    output_int=joinpath(path2files,"ComponentList_Aligned_int.csv")
    output_area=joinpath(path2files,"ComponentList_Aligned_area.csv")
    output_res=joinpath(path2files,"ComponentList_Aligned_res.csv")
    output_num=joinpath(path2files,"ComponentList_Aligned_compNum.csv")
    output_MS1mz=joinpath(path2files,"ComponentList_Aligned_MS1mz.csv")
    output_MS1int=joinpath(path2files,"ComponentList_Aligned_MS1int.csv")
    if !MS1only
        output_MS2mz=joinpath(path2files,"ComponentList_Aligned_MS2mz.csv")
        output_MS2int=joinpath(path2files,"ComponentList_Aligned_MS2int.csv")
    end

    CSV.write(output_int,table_int_f)
    CSV.write(output_area,table_are_f)
    CSV.write(output_res,table_res_f)
    CSV.write(output_num,table_num_f)
    CSV.write(output_MS1mz,table_MS1mz_f)
    CSV.write(output_MS1int,table_MS1int_f)
    if !MS1only
        CSV.write(output_MS2mz,table_MS2mz_f)
        CSV.write(output_MS2int,table_MS2int_f)
    end

    #XLSX.writetable(output, Intensities=(collect(DataFrames.eachcol(table_int_f)),
    # DataFrames.names(table_int_f)  ), Areas=(collect(DataFrames.eachcol(table_are_f)),
    #  DataFrames.names(table_are_f)  ), Resolutions=(collect(DataFrames.eachcol(table_res_f)),
    #   DataFrames.names(table_res_f)  ), overwrite=true)
    if MS1only
        return(table_int_f,table_are_f,table_res_f,table_num_f,table_MS1mz_f,table_MS1int_f)
    else
        return(table_int_f,table_are_f,table_res_f,table_num_f,table_MS1mz_f,table_MS1int_f,table_MS2mz_f,table_MS2int_f)
    end
end


#####################################################################################
# A function to align features 

function component_align(path2files,mz_tol::Float64=0.0,rt_tol::Float64=0.1,v_n_mz::String="",v_n_rt::String="",v_n_int::String="")

    if length(v_n_mz) > 0 && mz_tol > 0

        # External
        println("It is not yet possible to run component alignment on external files.")
        # component_align_external(path2files,mz_tol,rt_tol,v_n_mz,v_n_rt,v_n_int)

    elseif length(v_n_mz) == 0 && mz_tol == 0
        # Internal 
        println("These reports will be aligned as internally generated ones (i.e. via CompCreate).")
        component_align_internal(path2files)

    elseif length(v_n_mz) == 0 && mz_tol > 0
        # Internal with Set tolerances
        println("These reports will be aligned as internally generated ones (i.e. via CompCreate) with set mass and retention tolerance.")
        component_align_internal_wp(path2files,mz_tol,rt_tol)
        
    end 


    
end






###########################################
"""
# # test area ""

# path2files = "C:\\Users\\dherwer\\OneDrive - UvA\\Code single files\\Test - files\\" 
# mz_tol = 0.02
# rt_tol = 0.1

# # Internal
# component_align(path2files)

# # Internal with tols 
# component_align(path2files,mz_tol,rt_tol)

path2files = "C:\\Offline data\\Brain POS"
path2files = "C:\\Offline data\\Liver POS"
mz_tol = 0.005
rt_tol = 0.1
component_align_internal_parts(path2files, 10, 0.1, mz_tol, rt_tol)
"""