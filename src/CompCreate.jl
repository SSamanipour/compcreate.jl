__precompile__(false)
module CompCreate


using CSV
using Glob
using DataFrames
using Statistics
using JLD2
using BenchmarkTools
using ProgressBars
using XLSX
using LsqFit
using Dierckx
using FileIO
using SpecialFunctions
using BSON 



include("KMD.jl")
include("Component.jl")
include("CompAlign.jl")
include("Peak_finder.jl")

# Write your package code here.

export KMD_filter, KMD_filter_batch, KMD_calc, KMD_calc_ext, comp_ms1, comp_DIA_batch, comp_DDA_ESI, compcreate, comp_DIA, comp_DIA_ESI, comp_DIA_ESI_mc, comp_SWATH_ESI, component_align

end
